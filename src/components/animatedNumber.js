import React from 'react';
import PropTypes from 'prop-types';
import AnimateNumber from '../packages/react-native-animate-number';

const AnimatedNumber = ({ value, formatter, style }) => (
  <AnimateNumber timing="linear" interval={ 1 } value={ value } formatter={ val => `${parseFloat(val).toFixed(formatter)}` } style={ style } />
);
AnimatedNumber.propTypes = {
  style: PropTypes.object.isRequired,
  value: PropTypes.object.isRequired,
  formatter: PropTypes.number.isRequired,
};
export default AnimatedNumber;
