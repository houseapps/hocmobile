import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';

import PropTypes from 'prop-types';

import Image from './image';

import * as Statics from '../helpers/statics';

const BackButton = ({ navigator }) => {
  return (
    <TouchableOpacity
      onPress={ () => navigator.pop() }
      style={ styles.container }
    >
      <Image
        uri="backNavyBlue"
        height={ 20 }
        width={ 20 }
      />
    </TouchableOpacity>
  );
};

BackButton.propTypes = {
  navigator: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  container: {
    height: Statics.size(30),
    width: Statics.size(20),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});
export default BackButton;
