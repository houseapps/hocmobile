import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from './text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

const BankInfoItem = account => (
  <View style={ styles.accountList }>
    <View style={ styles.row }>
      <Text style={ styles.title }>Banka:</Text>
      <Text style={ styles.accountInfo }>{ account.bank_name }</Text>
    </View>
    <View style={ styles.row }>
      <Text style={ styles.title }>Hesap İsmi:</Text>
      <Text style={ styles.accountInfo }>{ account.account_name }</Text>
    </View>
    <View style={ styles.row }>
      <Text style={ styles.title }>IBAN:</Text>
      <Text selectable={ true } style={ styles.accountInfo }>{ account.iban }</Text>
    </View>
    <View style={ styles.row }>
      <Text style={ styles.title }>Hesap Kuru:</Text>
      <Text style={ styles.accountInfo }>{ account.asset }</Text>
    </View>
  </View>

);

export default BankInfoItem;

const styles = StyleSheet.create({
  accountList: {
    borderTopWidth: 1,
    borderTopColor: Colors.borderColor,
    paddingTop: Statics.size(20),
  },
  row: {
    flexDirection: 'row',
  },
  title: {
    fontSize: Statics.size(18),
    fontFamily: Fonts.PBold,
    color: Colors.navyBlue,
    marginRight: Statics.size(10),
  },
  accountInfo: {
    marginBottom: Statics.size(20),
    fontSize: Statics.size(18),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
});
