import React from 'react';
import { Image } from 'react-native';
import * as Statics from '../helpers/statics';
import Assets from '../helpers/assets';

const Img = () => {
  const imageStyle = {
    height: Statics.size(300),
    width: Statics.WIDTH,
    resizeMode: 'stretch',
    position: 'absolute',
    top: Statics.size(40),
    left: 0,
    zIndex: -9,
  };
  return (
    <Image
      source={ Assets.bgImage }
      style={ imageStyle }
    />
  );
};

export default Img;
