import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';
import PropTypes from 'prop-types';
import moment from 'moment';

import DateTimePicker from '../components/dateTimePicker';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import UIC from '../stores/uiControllerStore';

const BirthDayInput = ({ onConfirm, value, ...props }) => {
  return (
    <TouchableOpacity
      onPress={ () => UIC.openDateTimePicker() }
      style={ styles.container }
      { ...props }
    >
      {!value ?
        <Text style={ styles.text }>Doğum Tarihi</Text>
        :
        <Text style={ styles.text }>{ moment(value).format('L') }</Text>
      }
      <DateTimePicker
        isVisible={ UIC.dateTimePicker }
        onCancel={ () => UIC.closeDateTimePicker() }
        onConfirm={ (v) => {
          onConfirm(v);
          UIC.closeDateTimePicker();
        } }
      />
    </TouchableOpacity>
  );
};

BirthDayInput.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    // marginTop: Statics.size(20),
    height: Statics.size(60),
    width: Statics.WIDTH * 0.829,
    borderRadius: Statics.size(3),
    // overflow: 'hidden',
    alignItems: 'center',
    borderWidth: 1,
    // borderRadius: Statics.size(4),
    borderColor: 'transparent',
    backgroundColor: 'white',
    borderBottomWidth: 0,
    marginBottom: Statics.size(30),
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    // marginBottom: Statics.size(30),
    // backgroundColor: 'rgb(43, 144, 255)',
  },
  text: {
    color: Colors.navyBlue,
    fontSize: Statics.size(19),
    fontWeight: '400',
    marginLeft: 10,
  },
});

export default observer(BirthDayInput);
