
import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import * as Statics from '../helpers/statics';
import Text from './text';

const Button = ({
  label,
  labelColor,
  backgroundColor,
  height,
  width,
  fontSize = Statics.size(14),
  fontWeight = '400',
  onPress,
  ...props
}) => {
  const buttonStyle = {
    height: Statics.size(height),
    width,
    backgroundColor,
  };
  const labelStyle = {
    color: labelColor,
    fontSize,
    fontWeight,
  };
  return (
    <TouchableOpacity onPress={ () => onPress() } style={ [styles.button, buttonStyle, { ...props }] }>
      <Text style={ labelStyle }>{label}</Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  labelColor: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  fontSize: PropTypes.number.isRequired,
  fontWeight: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    alignSelf: 'center',
  },
});


export default Button;
