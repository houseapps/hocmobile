import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import Image from '../components/image';
import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

const Checkbox = ({ onPress, isSelected = false }) => {
  return (
    <View>
      <TouchableOpacity
        style={ [isSelected ? styles.selectedButton : styles.unSelectedButton] }
        activeOpacity={ 1 }
        onPress={ () => onPress() }
      >
        {isSelected &&
          <Image
            onPress={ () => onPress() }
            style={ styles.image }
            uri="checkbox"
            height={ Statics.size(24) }
            width={ Statics.size(24) }
          />
        }
      </TouchableOpacity>
    </View>
  );
};

Checkbox.propTypes = {
  onPress: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

const styles = StyleSheet.create({
  unSelectedButton: {
    height: Statics.size(24),
    width: Statics.size(24),
    borderRadius: Statics.size(3),
    borderColor: Colors.navyBlue,
    borderWidth: 1,
    // backgroundColor: 'rgb(43, 144, 255)',
  },
  selectedButton: {
    height: Statics.size(24),
    width: Statics.size(24),
    borderRadius: Statics.size(3),
  },
  image: {
    borderRadius: Statics.size(3),
  },
});

export default Checkbox;
