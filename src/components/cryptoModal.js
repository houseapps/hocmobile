import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

import { observer } from 'mobx-react/native';

import Modal from 'react-native-modal';

import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import CryptoDepositForm from '../stores/forms/cryptoDepositForm';
import NavigationStore from '../stores/navigationStore';
import { navigatorStyle } from '../consts/navigatorStyle';

import UIC from '../stores/uiControllerStore';
import OperationStore from '../stores/operationStore';

@observer
export default class CryptoModal extends Component {
    static navigatorStyle = navigatorStyle;
    route(name, screen) {
      CryptoDepositForm.form.asset = name;
      UIC.closeCryptoModal();
      NavigationStore.route(screen, false, {}, this.props.nav, 'Crypto');
    }
    render() {
      const { screen } = this.props;
      return (
        <Modal
          isVisible={ UIC.cryptoModal }
          onBackButtonPress={ () => UIC.closeCryptoModal() }
          onBackdropPress={ () => UIC.closeCryptoModal() }
          onSwipe={ () => UIC.closeCryptoModal() }
        >
          <View style={ styles.contentContainer }>
            {CryptoDepositForm.cryptoList.map((item, index) => {
              return (
                <TouchableOpacity
                  key={ item }
                  onPress={ () => this.route(item, screen) }
                  style={ [styles.itemContainer, index && styles.border] }
                >
                  <Image
                    uri={ item.toLowerCase() }
                    width={ 45 }
                    height={ 45 }
                    style={ styles.currencyIcon }
                  />
                  <Text style={ styles.currencyText }>{ OperationStore.productsLongNames[item]}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </Modal>
      );
    }
}

const styles = StyleSheet.create({
  contentContainer: {
    backgroundColor: 'white',
    maxHeight: Statics.HEIGHT - Statics.size(150),
    width: Statics.WIDTH - Statics.size(60),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Statics.size(5),
    // paddingTop: Statics.size(50),
    // paddingBottom: Statics.size(50),
  },
  border: {
    borderTopWidth: 1,
    borderColor: '#e0e0e0',
  },
  itemContainer: {
    height: Statics.size(80),
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  currencyIcon: {
    marginLeft: Statics.size(15),
    resizeMode: 'contain',
  },
  currencyText: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    marginRight: Statics.size(15),
  },
});

