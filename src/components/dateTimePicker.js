import React from 'react';
import DateTimePicker from 'react-native-modal-datetime-picker';

import PropTypes from 'prop-types';

const DTP = ({ isVisible, onConfirm, onCancel }) => {
  return (
    <DateTimePicker
      isVisible={ isVisible }
      onConfirm={ value => onConfirm(value) }
      onCancel={ () => onCancel() }
    />
  );
};

DTP.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default DTP;

