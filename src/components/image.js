
import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';

import * as Statics from '../helpers/statics';
import Assets from '../helpers/assets';

const Img = ({
  uri,
  height,
  width,
  style,
  resizeMode = 'stretch',
  base64data = null,
  url = null,
}) => {
  const imageStyle = {
    height: Statics.size(height),
    width: Statics.size(width),
    resizeMode,
  };
  if (url) {
    return(
    <Image
    source={ { uri: url } }
    style={ [imageStyle, style] }
  />
    );
  } else if (base64data) {
    return (
      <Image
        source={ { uri: base64data } }
        style={ [imageStyle, style] }
      />
    );
  } else {
    return (
      <Image
        source={ Assets[uri] }
        style={ [imageStyle, style] }
      />
    );
  }
};

Img.propTypes = {
  style: PropTypes.object.isRequired,
  uri: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
};

export default Img;
