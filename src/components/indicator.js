import React from 'react';
import { View, StyleSheet } from 'react-native';
import { observer } from 'mobx-react/native';
import { BarIndicator } from 'react-native-indicators';
import Modal from 'react-native-modal';

import UIC from '../stores/uiControllerStore';

import Text from './text';
import Button from './button';
import PaparaWebViewModal from './paparaWebViewModal';


import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import Image from './image';

const Indicator = ({ isForever = false }) => {
  let isDisplayBar = false;
  if (isForever) isDisplayBar = true;
  if (UIC.indicator) isDisplayBar = true;
  return (
    <Modal isVisible={ UIC.indicator || UIC.alertState.isVisible || isForever }>
      {UIC.paparaWebView && <PaparaWebViewModal /> }
      {UIC.alertState.isVisible ?
        <View
          style={ [
            styles.messageContainer,
            UIC.alertState.type === 0 ?
              styles.success
              :
              styles.error] }
        >
          <Image
            uri={ UIC.alertState.type === 0 ? 'success' : 'error' }
            height={ Statics.size(120) }
            width={ Statics.size(120) }
          />
          <Text style={ styles.alertTypeText }>
            {UIC.alertState.type === 0 ? 'BAŞARILI' : 'HATA'}
          </Text>
          <Text style={ styles.alertMessage }>
            {UIC.alertState.message}
          </Text>
          <Button
            height={ Statics.size(40) }
            width={ Statics.size(200) }
            label={ UIC.alertState.type === 0 ? 'Devam et' : 'Tekrar dene' }
            labelColor={ Colors.navyBlue }
            backgroundColor="white"
            fontSize={ Statics.size(21) }
            onPress={ () => {
              UIC.alertState.callback();
              UIC.closeIndicatorPopup();
              UIC.closeAlertPopup();
            } }
          />
        </View>
        :
        isDisplayBar && !UIC.paparaWebView && <BarIndicator color="white" />
      }
    </Modal>
  );
};

const styles = StyleSheet.create({
  messageContainer: {
    height: Statics.size(375),
    width: Statics.size(275),
    alignItems: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#323030',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    justifyContent: 'space-between',
    padding: Statics.size(30),
  },
  error: {
    backgroundColor: '#F26051',
  },
  success: {
    backgroundColor: '#20DA9B',
  },
  alertTypeText: {
    fontSize: Statics.size(30),
    color: 'white',
  },
  alertMessage: {
    fontSize: Statics.size(17),
    color: 'white',
    textAlign: 'center',
  },
});

export default observer(Indicator);
