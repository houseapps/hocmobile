import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';

import Image from '../components/image';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import UserStore from '../stores/userStore';

@observer
export default class NavigationBar extends Component {
  render() {
    return (
      <View style={ styles.container }>
        <View style={ styles.leftView } />
        <View style={ styles.middleView }>
          <Image
            uri="hoclogo"
            width={ Statics.size(150) }
            height={ Statics.size(25) }
          />
        </View>
        <View style={ styles.rightView }>
          <TouchableOpacity
            onPress={ () => UserStore.logout() }
          >
            <Image
              uri="notification"
              width={ Statics.size(24) }
              height={ Statics.size(28) }
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: Statics.size(45),
    width: Statics.WIDTH - Statics.size(20),
    backgroundColor: Colors.blue,
    // justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftView: {
    flex: 2,
    height: Statics.size(45),
    backgroundColor: Colors.blue,
  },
  middleView: {
    flex: 8,
    backgroundColor: Colors.blue,
    height: Statics.size(45),
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightView: {
    flex: 2,
    height: Statics.size(45),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.blue,
  },
});
