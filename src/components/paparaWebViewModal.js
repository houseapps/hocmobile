import React from 'react';
import { StyleSheet, WebView, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';

import UIC from '../stores/uiControllerStore';

import * as Statics from '../helpers/statics';

import PaparaDepositForm from '../stores/forms/paparaDepositForm';

const PaparaWebViewModal = () => {
  const { paymentUrl } = PaparaDepositForm.form;
  console.log('paymentUrl', paymentUrl);
  const callbackJSCode = `document.body.style.zoom = "100%"; setTimeout(function(){window.postMessage(document.getElementsByTagName('div')[0].getAttribute('id'))}, 1000);`;
  return (
    <TouchableOpacity
      activeOpacity={ 1 }
      style={ styles.modalContainer }
      onPress={ () => {
        UIC.closeIndicatorPopup();
        UIC.closePaparaWebViewModal();
      } }
    >
      <TouchableOpacity
        activeOpacity={ 1 }
        style={ styles.contentContainer }
      >
        <WebView
          javaScriptEnabled
          domStorageEnabled
          source={ { uri: paymentUrl } }
          onMessage={ data => PaparaDepositForm.onWebViewMessage(data) }
          javaScriptEnabledAndroid
          injectedJavaScript={ callbackJSCode }
        />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: Statics.WIDTH,
    height: Statics.HEIGHT,
    backgroundColor: 'transparent',
  },
  contentContainer: {
    height: Statics.size(420),
    width: Statics.WIDTH - Statics.size(50),
    alignSelf: 'center',
  },
});

export default observer(PaparaWebViewModal);
