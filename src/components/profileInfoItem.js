import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from './text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

const ProfileInfoItem = props => (
  <View style={ styles.row }>
    <Text style={ styles.title }>{ props.title }</Text>
    <Text style={ styles.accountInfo }>{ props.value }</Text>
  </View>

);

export default ProfileInfoItem;

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  title: {
    fontSize: Statics.size(18),
    fontFamily: Fonts.PBold,
    color: Colors.navyBlue,
    marginRight: Statics.size(10),
  },
  accountInfo: {
    marginBottom: Statics.size(20),
    fontSize: Statics.size(18),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
});
