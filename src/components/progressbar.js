import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { observer } from 'mobx-react/native';

import ProgressBar from 'react-native-progress/Bar';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import UserStore from '../stores/userStore';

@observer
class Progressbar extends Component {
  componentDidMount() {
    const time = 5;

    this.interval = setInterval(() => {
      if (UserStore.timer > 1) {
        // set statein callbackine start interval
        UserStore.timer = 0;
        this.props.onTrigger();
      } else {
        UserStore.timer = UserStore.timer + 1 / (time * 10);
      }
    }, 100);
  }
  render() {
    const {
      color = Colors.turquoise,
      borderWidth = 0,
      borderRadius = 0,
      width = 0,
    } = this.props;
    return (
      <View style={ styles.container }>
        <ProgressBar
          progress={ UserStore.timer }
          style={ styles.progressBar }
          color={ color }
          borderWidth={ borderWidth }
          borderRadius={ borderRadius }
          width={ width }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  progressBar: {
    flex: 1,
    height: Statics.size(3),
  },
  container: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
  },
});

export default Progressbar;
