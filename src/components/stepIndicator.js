
import React from 'react';
import StepIndicator from 'react-native-step-indicator';
import PropTypes from 'prop-types';
import Colors from '../helpers/colors';

const labels = ['Önyüz', 'Arkayüz', 'Profil'];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 1,
  currentStepStrokeWidth: 1,
  stepStrokeCurrentColor: Colors.navyBlue,
  stepStrokeWidth: 1,
  stepStrokeFinishedColor: Colors.navyBlue,
  stepStrokeUnFinishedColor: 'grey',
  separatorFinishedColor: Colors.green,
  separatorUnFinishedColor: Colors.navyBlue,
  stepIndicatorFinishedColor: Colors.green,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: Colors.green,
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fff',
  stepIndicatorLabelFinishedColor: '#fff',
  stepIndicatorLabelUnFinishedColor: Colors.navyBlue,
  labelColor: Colors.navyBlue,
  labelSize: 13,
  currentStepLabelColor: Colors.navyBlue,
};

const SIndicator = ({
  currentPosition = 0,
}) => {
  return (
    <StepIndicator
      customStyles={ customStyles }
      currentPosition={ currentPosition }
      labels={ labels }
      stepCount={ 3 }
    />
  );
};

SIndicator.propTypes = {
  currentPosition: PropTypes.number.isRequired,
};

export default SIndicator;
