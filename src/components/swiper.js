import React, { Component } from 'react';
import Swiper from 'react-native-swiper';

import * as Statics from '../helpers/statics';

class SWPR extends Component {
  render() {
    return (
      <Swiper
        ref={ r => this.props.reference(r) }
        showsButtons={ false }
        scrollEnabled={ false }
        showsPagination={ false }
        onIndexChanged={ index => this.props.onIndexChanged(index) }
        height={ this.props.height }
        width={ this.props.width }
        { ...this.props }
      >
        {this.props.children}
      </Swiper>
    );
  }
}
export default SWPR;
