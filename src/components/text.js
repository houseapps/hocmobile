import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import Fonts from '../helpers/fonts';

const CText = ({ style, ...props }) =>
  <Text allowFontScaling={ false } style={ [styles.fontFamily, style] } { ...props } />;

const styles = StyleSheet.create({
  fontFamily: {
    fontFamily: Fonts.PLight,
    includeFontPadding: false,
  },
});

CText.propTypes = {
  style: PropTypes.object.isRequired,
};

export default CText;
