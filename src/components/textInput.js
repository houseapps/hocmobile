import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import Sae from '../components/sae';
import Image from '../components/image';
import * as Statics from '../helpers/statics';
import Fonts from '../helpers/fonts';
import Colors from '../helpers/colors';

const textInput = ({
  label,
  onChangeText,
  onPressEye,
  isVisible = true,
  eye = false,
  eyeState = false,
  inputStyle,
  labelStyle,
  textStyle,
  containerStyle,
  keyboardType = 'default',
  value = '',
  masked = false,
  maskFormat = '(999) 999 ',
  maskType = 'cel-phone',
  maskLength = 14,
  halfWidth = false,
  editable = true,
  autoCapitalize = 'none',
  ...props
}) => {
  return (
    <View style={ !halfWidth ? styles.containerDesign : styles.containerDesignHalf }>
      <View
        style={ [styles.container, { ...containerStyle }] }
        { ...props }
      >
        {!editable &&
          <View style={ [styles.container,{ ...containerStyle }, styles.disabledUI] }></View>
        }
        <Sae
          label={ label }
          // this is used as active border color
          borderColor="transparent"
          secureTextEntry={ !isVisible }
          // this is used to set backgroundColor of label mask.
          // please pass the backgroundColor of your TextInput container.
          // backgroundColor="transparent"
          // default textipnut value getter method
          autoCorrect={ false }
          onChangeText={ text => onChangeText(text) }
          // input style
          style={ [styles.input, inputStyle] }
          // color="white"
          value={ value }
          fontWeight="400"
          labelStyle={ [styles.labelStyle, labelStyle] }
          inputStyle={ [styles.inputStyle, textStyle] }
          selectionColor={ Colors.navyBlue }
          keyboardType={ keyboardType }
          masked={ masked }
          maskFormat={ maskFormat }
          maskType={ maskType }
          maskLength={ maskLength }
          autoCapitalize= { autoCapitalize }
        />
        {eye &&
          <TouchableOpacity onPress={ () => onPressEye() } style={ styles.eyeContainer }>
            <Image uri={ !eyeState ? 'eyeOpen' : 'eyeClose' } height={ 18 } width={ 18 } />
          </TouchableOpacity>
        }
      </View>
    </View>
  );
};

textInput.propTypes = {
  label: PropTypes.string.isRequired,
  keyboardType: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onPressEye: PropTypes.func.isRequired,
  eye: PropTypes.bool.isRequired,
  isVisible: PropTypes.bool.isRequired,
  inputStyle: PropTypes.object.isRequired,
  containerStyle: PropTypes.object.isRequired,
  value: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: Statics.size(60),
    width: Statics.WIDTH * 0.829,
    borderRadius: Statics.size(3),
    overflow: 'hidden',
    alignItems: 'flex-end',
    // backgroundColor: 'rgb(43, 144, 255)',
    backgroundColor: 'white',
  },
  input: {
    width: Statics.size(Statics.WIDTH * 0.829 - Statics.size(60)),
    height: Statics.size(45),
    borderRadius: Statics.size(3),
  },
  labelStyle: {
    color: Colors.navyBlue,
    opacity: 0.7,
    fontFamily: Fonts.PThin,
    fontWeight: '300',
  },
  inputStyle: {
    color: Colors.navyBlue,
    fontWeight: '400',
    fontFamily: Fonts.PLight,
  },
  eyeContainer: {
    height: Statics.size(60),
    width: Statics.size(60),
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerDesign: {
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    backgroundColor: 'white',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 6,
    marginBottom: Statics.size(30),
    width: Statics.WIDTH * 0.829,
    alignSelf: 'center',
  },
  containerDesignHalf: {
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    backgroundColor: 'white',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 6,
    marginBottom: Statics.size(30),
    width: (Statics.size(Statics.WIDTH * 0.829 - Statics.size(10)) / 2),
    alignSelf: 'center',
  },
  disabledUI: {
    position: 'absolute',
    zIndex: 999,
    top: 0,
    left: 0,
    backgroundColor: 'transparent'
  }
});

export default textInput;
