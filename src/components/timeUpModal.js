import React from 'react';
import { View, StyleSheet } from 'react-native';
import { observer } from 'mobx-react/native';
import { BarIndicator } from 'react-native-indicators';
import Modal from 'react-native-modal';

import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';

import Text from './text';
import Button from './button';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import Image from './image';

const TimeUpModal = () => {
  return (
    <Modal isVisible={ UIC.timeUp }>
      <View
        style={ [
          styles.messageContainer,
          styles.error] }
      >
        <Image
          uri="error"
          height={ Statics.size(120) }
          width={ Statics.size(120) }
        />
        <Text style={ styles.alertTypeText }>
          SÜRE BİTTİ
        </Text>
        <Text style={ styles.alertMessage }>
          Sms olarak gönderilen onay kodunu girmeniz gerekiyor. Tekrar gönderilsin mi ?
        </Text>
        <Button
          height={ Statics.size(40) }
          width={ Statics.size(200) }
          label="Gönder"
          labelColor={ Colors.navyBlue }
          backgroundColor="white"
          fontSize={ Statics.size(21) }
          onPress={ () => {
            UserStore.sendCode();
          } }
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  messageContainer: {
    height: Statics.size(375),
    width: Statics.size(275),
    // justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#323030',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    justifyContent: 'space-between',
    padding: Statics.size(30),
  },
  error: {
    backgroundColor: '#F26051',
  },
  alertTypeText: {
    fontSize: Statics.size(30),
    color: 'white',
  },
  alertMessage: {
    fontSize: Statics.size(17),
    color: 'white',
    textAlign: 'center',
  },
});

export default observer(TimeUpModal);
