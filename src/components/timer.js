import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

let timer = () => {};
const TIME = 120;
class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remainingTime: TIME,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.state) {
      this.countdownTimer();
    }
  }
  countdownTimer() {
    this.setState({ remainingTime: TIME });
    clearInterval(timer);
    timer = setInterval(() => {
      if (!this.state.remainingTime) {
        this.props.onEnd();
        clearInterval(timer);
        return false;
      }
      this.setState((prevState) => {
        return { remainingTime: prevState.remainingTime - 1 };
      });
    }, 1000);
  }
  renderClockFormat() {
    const { remainingTime } = this.state;

    const time = new Date(remainingTime * 1000);
    const minute = time.getMinutes().toString().length === 1 ? `0${time.getMinutes()}` : time.getMinutes();

    const second = time.getSeconds().toString().length === 1 ? `0${time.getSeconds()}` : time.getSeconds();
    const display = `${minute}:${second}`;

    return display;
  }
  render() {
    return (
      <View style={ styles.container }>
        <Text style={ styles.text }>{this.renderClockFormat()}</Text>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: Statics.size(40),
    color: Colors.red,
  },
});

export default Timer;
