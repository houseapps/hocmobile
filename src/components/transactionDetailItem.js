import React from 'react';
import { View, StyleSheet } from 'react-native';

import Text from './text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

const TransactionItemDetailItem = ({ data }) => {
  const date = data.created_at.replace(', ', '\n');
  return (
    <View style={ styles.detailContainer }>
      <View style={ styles.dateSection }>
        <Text style={ styles.dateText }>{ date.replace(' ', '\n') }</Text>
      </View>
      <View style={ styles.transactionSection }>
        <View style={ styles.occuredBaseContainer }>
          <Text style={ styles.occuredBaseText }> Birim { data.occured_base_amount }</Text>
          <Text style={ styles.occuredBaseText }>{ data.base }</Text>
        </View>
        <View style={ styles.quoteSection }>
          <Text style={ styles.quoteText }>{ data.quote_amount }</Text>
          <Text style={ styles.quoteText }> { data.quote }</Text>
        </View>
      </View>
      <View style={ styles.baseSection }>
        <Text style={ styles.baseText }>{ data.base_amount }</Text>
        <Text style={ styles.baseText }> { data.base }</Text>
      </View>
    </View>

  );
};

export default TransactionItemDetailItem;

const styles = StyleSheet.create({
  detailContainer: {
    flex: 1,
    flexDirection: 'row',
    height: Statics.size(90),
    width: '100%',
    borderBottomWidth: 1,
    borderColor: Colors.borderColor,
    paddingTop: Statics.size(8),
    paddingBottom: Statics.size(8),
  },
  dateSection: {
    width: '15%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderRightWidth: 1,
    borderColor: Colors.borderColor,
  },
  dateText: {
    fontSize: Statics.size(13),
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
    marginTop: Statics.size(5),
  },
  occuredBaseContainer: {
    flexDirection: 'row',
  },
  occuredBaseText: {
    fontSize: Statics.size(13),
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
    marginBottom: Statics.size(8),
    marginTop: Statics.size(5),
    marginRight: 2,
  },
  quoteSection: {
    flexDirection: 'row',
  },
  quoteText: {
    fontSize: Statics.size(16),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  transactionSection: {
    width: '50%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRightWidth: 1,
    borderColor: Colors.borderColor,
  },
  baseSection: {
    width: '35%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  baseText: {
    fontSize: Statics.size(16),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginBottom: Statics.size(20),
  },
});
