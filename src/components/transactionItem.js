import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

import Text from './text';
import Image from './image';

import TransactionForm from '../stores/forms/transactionForm';
import NavigationStore from '../stores/navigationStore';
import TransactionStore from '../stores/transactionStore';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

export default class TransactionItem extends Component {
  pressHandler(asset) {
    TransactionStore.fetchAssetDetail(asset).then(() => {
      NavigationStore.route('TransactionDetailScreen', false, {}, this.props.navigator, asset);
    });
  }

  render() {
    const { totalBalanceAsUSD } = TransactionForm;
    const { asset, final_balance } = this.props;

    return (
      <TouchableOpacity onPress={ () => this.pressHandler(asset) } style={ styles.contentContainer }>
        <View style={ styles.assetTypeView }>
          <Image
            uri={ asset.toLowerCase() }
            width={ 36 }
            height={ 36 }
          />
          <Text style={ styles.assetTypeLabel }>{asset}</Text>
        </View>
        <View style={ styles.assetBalanceView }>
          <View style={ styles.valuesView }>
            <Text style={ styles.totalBalance }>{final_balance}</Text>
            <Text style={ styles.totalBalanceAsUSD }>
              {totalBalanceAsUSD(asset, final_balance)} USD
            </Text>
          </View>
          <Image
            uri="triangleRightGray"
            width={ 14 }
            height={ 14 }
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flexDirection: 'row',
    height: Statics.size(100),
    marginLeft: Statics.size(20),
    marginRight: Statics.size(20),
    borderBottomWidth: 1,
    borderColor: Colors.borderColor,
  },
  assetTypeView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  assetBalanceView: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  assetTypeLabel: {
    marginLeft: Statics.size(10),
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
  },
  totalBalance: {
    textAlign: 'right',
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  totalBalanceAsUSD: {
    textAlign: 'right',
    fontSize: Statics.size(16),
    color: Colors.navyBlue,
  },
  valuesView: {
    marginRight: Statics.size(10),
  },
});
