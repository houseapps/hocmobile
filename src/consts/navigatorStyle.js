import Colors from '../helpers/colors';
import * as Statics from '../helpers/statics';
import Assets from '../helpers/assets';

export const navigatorStyle = {
  navBarTextColor: '#fff',
  navBarBackgroundColor: Colors.blue,
  navBarTitleTextCentered: true,
  topBarElevationShadowEnabled: false,
  topBarBorderColor: 'transparent',
  topBarBorderWidth: Statics.size(5.5),
  screenBackgroundColor: 'white',
};

export const leftButtons = [
  {
    id: 'back',
    icon: Assets.back,
    buttonFontSize: 16,
    disableIconTint: true,
    disabled: false,
  },
];

export const rightButtons = [
  {
    id: 'notification',
    icon: Assets.notification,
    buttonFontSize: 16,
    disableIconTint: true,
    disabled: false,
  },
];
