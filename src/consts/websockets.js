export const BTCSOCKET = 'wss://stream.binance.com:9443/ws/btcusdt@ticker';
export const LTCSOCKET = 'wss://stream.binance.com:9443/ws/ltcusdt@ticker';
export const ETHSOCKET = 'wss://stream.binance.com:9443/ws/ethusdt@ticker';
export const XRPSOCKET = 'wss://stream.binance.com:9443/ws/xrpusdt@ticker';