import React from 'react';
import UserStore from '../stores/userStore';

const HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

const BASE_URL = 'http://18.184.6.56/api/';

class API {
  request(method, path, body, isFullPath = false) {
    return new Promise((resolve, reject) => {
      let url = `${BASE_URL}${path}`;
      const requestParams = {
        method,
        headers: HEADERS,
        body: undefined,
      };
      const { user } = UserStore;
      if (user) {
        requestParams.headers.Authorization = `Token ${user.token}`;
      }
      if (body) {
        requestParams.body = JSON.stringify(body);
      }
      url = isFullPath ? path : url;
      console.log('requestParams', requestParams);
      fetch(url, requestParams)
        .then((response) => {
          console.log('response', response);
          if (response.status === 401) {
            return UserStore.logout();
          }
          if (response.status < 300) {
            if (response._bodyInit === '') {
              return resolve(true);
            } else {
              return resolve(response.json());
            }
          } else {
            response.json().then((errorResponse) => {
              return reject(errorResponse);
            }).catch((error) => {
              return reject(error);
            });
          }
        }).catch((err) => {
          return reject(err);
        });
    });
  }
}

const api = new API();
export default api;
