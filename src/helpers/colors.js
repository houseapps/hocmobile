const colors = {
  turquoise: 'rgb(0, 255, 255)',
  black: 'rgb(0, 0, 0)',
  fadedBlack: 'rgba(0, 0, 0, 0.6)',
  blue: 'rgb(0, 122, 255)',
  navyBlue: 'rgb(32, 55, 112)',
  green: 'rgb(131, 192, 92)',
  red: 'rgb(255, 59, 48)',
  borderColor: '#BBC2D3',
  profileColors: {
    profileInfo: '#00FFFF',
    changePassword: '#00BEFF',
    idVerification: '#007AFF',
    smsVerification: '#203770',
    logout: '#ff3b30',
  },
};

export default colors;
