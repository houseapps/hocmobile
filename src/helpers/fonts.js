const fonts = {
  PThin: 'ProximaNovaT-Thin',
  PLight: 'ProximaNova-Light',
  PBlack: 'ProximaNova-Black',
  PBold: 'ProximaNova-Bold',
  PRegular: 'ProximaNova-Regular',
};
export default fonts;
