import { Navigation } from 'react-native-navigation';

import UserStore from '../stores/userStore';

import Assets from '../helpers/assets';
import Colors from '../helpers/colors';

import IDInfoVerificationScreen from '../views/idInfoVerification';
import IDCardVerificationScreen from '../views/idCardVerification';
import TransactionDetailScreen from '../views/transactionDetail';
import TransactionsListScreen from '../views/transactionsList';
import SmsVerificationScreen from '../views/smsVerification';
import ChangePasswordScreen from '../views/changePassword';
import PaparaWithdrawScreen from '../views/paparaWithdraw';
import CryptoWithdrawScreen from '../views/cryptoWithdraw';
import ResetPasswordScreen from '../views/resetPassword';
import PaparaDepositScreen from '../views/paparaDeposit';
import CryptoDepositScreen from '../views/cryptoDeposit';
import EFTWithdrawScreen from '../views/eftWithdraw';
import ProfileInfoScreen from '../views/profileInfo';
import EFTDepositScreen from '../views/eftDeposit';
import DepositQRScannerScreen from '../views/depositQRScanner';
import WithdrawQRScannerScreen from '../views/withdrawQRScanner';
import PurchasesScreen from '../views/purchases';
import NavigationBar from '../components/navBar';
import RegisterScreen from '../views/register';
import PaymentsScreen from '../views/payments';
import ProfileScreen from '../views/profile';
import LoginScreen from '../views/login';
import HomeScreen from '../views/home';

export function registerComponents() {
  Navigation.registerComponent('IDInfoVerificationScreen', () => IDInfoVerificationScreen);
  Navigation.registerComponent('IDCardVerificationScreen', () => IDCardVerificationScreen);
  Navigation.registerComponent('TransactionDetailScreen', () => TransactionDetailScreen);
  Navigation.registerComponent('TransactionsListScreen', () => TransactionsListScreen);
  Navigation.registerComponent('SmsVerificationScreen', () => SmsVerificationScreen);
  Navigation.registerComponent('ChangePasswordScreen', () => ChangePasswordScreen);
  Navigation.registerComponent('PaparaWithdrawScreen', () => PaparaWithdrawScreen);
  Navigation.registerComponent('CryptoWithdrawScreen', () => CryptoWithdrawScreen);
  Navigation.registerComponent('ResetPasswordScreen', () => ResetPasswordScreen);
  Navigation.registerComponent('PaparaDepositScreen', () => PaparaDepositScreen);
  Navigation.registerComponent('CryptoDepositScreen', () => CryptoDepositScreen);
  Navigation.registerComponent('ProfileInfoScreen', () => ProfileInfoScreen);
  Navigation.registerComponent('EFTWithdrawScreen', () => EFTWithdrawScreen);
  Navigation.registerComponent('EFTDepositScreen', () => EFTDepositScreen);
  Navigation.registerComponent('DepositQRScannerScreen', () => DepositQRScannerScreen);
  Navigation.registerComponent('WithdrawQRScannerScreen', () => WithdrawQRScannerScreen);
  Navigation.registerComponent('PurchasesScreen', () => PurchasesScreen);
  Navigation.registerComponent('RegisterScreen', () => RegisterScreen);
  Navigation.registerComponent('PaymentsScreen', () => PaymentsScreen);
  Navigation.registerComponent('NavigationBar', () => NavigationBar);
  Navigation.registerComponent('ProfileScreen', () => ProfileScreen);
  Navigation.registerComponent('LoginScreen', () => LoginScreen);
  Navigation.registerComponent('HomeScreen', () => HomeScreen);
}

export function startApp() {
  UserStore.getUser().then(() => {
    UserStore.fetchProfile().then(() => {
      const { user } = UserStore.user;
      if (!user.id_verified) {
        this.startApplication('IDInfoVerificationScreen');
      } else if (!user.phone_number_verified) {
        this.startApplication('SmsVerificationScreen');
      } else this.switchToTabBased();
    }).catch(() => {
      this.switchToTabBased();
    });
  }).catch(() => {
    this.startApplication('LoginScreen');
  });
}

export function startApplication(screen) {
  Navigation.startSingleScreenApp({
    screen: {
      screen,
      navigatorStyle: {
        navBarHidden: true,
        screenBackgroundColor: 'white',
      },
      navigatorButtons: {},
    },
    appStyle: {
      orientation: 'portrait',
    },
    passProps: {},
    animationType: 'none',
  });
}

export function switchToTabBased() {
  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Varlıklar',
        screen: 'TransactionsListScreen',
        title: ' Varlıklar ',
        icon: Assets.transactionsIcon,
      },
      {
        label: 'Yatır',
        screen: 'PaymentsScreen',
        title: ' Yatır ',
        icon: Assets.sellIcon,
      },
      {
        label: 'Anasayfa',
        screen: 'HomeScreen',
        title: ' Anasayfa ',
        icon: Assets.homeIcon,
      },
      {
        label: 'Çek',
        screen: 'PurchasesScreen',
        title: ' Çek ',
        icon: Assets.buyIcon,
      },
      {
        label: 'Profilim',
        screen: 'ProfileScreen',
        title: ' Profilim ',
        icon: Assets.profileIcon,
      },
    ],
    portraitOnlyMode: true,
    tabsStyle: {
      initialTabIndex: 2,
      tabBarButtonColor: 'rgb(187, 194, 211)',
      tabBarLabelColor: 'gray',
      tabBarSelectedButtonColor: 'rgb(32, 55, 112)',
      navBarComponentAlignment: 'center',
      tabBarTranslucent: false,
      forceTitlesDisplay: true,
    },
    appStyle: {
      initialTabIndex: 2,
      tabBarTranslucent: false,
      forceTitlesDisplay: true,
      navBarComponentAlignment: 'center',
      navBarBackgroundColor: Colors.blue,
      tabBarButtonColor: 'rgb(187, 194, 211)',
      tabBarLabelColor: 'gray',
      tabBarSelectedButtonColor: 'rgb(32, 55, 112)',
    },
    animationType: 'none',
  });
}

export function routerWithoutNavbar(screen) {
  Navigation.startSingleScreenApp({
    screen: {
      screen,
      navigatorStyle: {
        navBarHidden: false,
        tabBarVisible: false,
        screenBackgroundColor: 'white',
      },
      navigatorButtons: {
      },
    },
    appStyle: {
      orientation: 'portrait',
    },
    passProps: {},
    animationType: 'none',
  });
}
