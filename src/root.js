import React from 'react';
import { Platform } from 'react-native';
import OneSignal from './packages/react-native-onesignal';
import * as Router from './helpers/router';
import { appID } from './consts/onesignal';
import BinanceStore from './stores/binanceStore';

Router.registerComponents();

BinanceStore.sockets();
BinanceStore.openusdtryHttpRequest();

console.disableYellowBox = true;

if (Platform.OS === 'android') OneSignal.init(appID);

Router.startApp();

