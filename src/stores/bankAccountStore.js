import React from 'react';
import * as mobx from 'mobx';

import API from '../helpers/api';

import { BANK_ACCOUNT_LIST } from '../helpers/endpoints';

const {
  observable,
  action,
} = mobx;

class BankAccountStore {
  @observable bankAccountList = [];
  @observable code = null;


  @action fetchBankAccounts() {
    return new Promise((resolve, reject) => API.request('GET', BANK_ACCOUNT_LIST)
      .then((response) => {
        this.bankAccountList = response.accounts;
        this.code = response.code;
      }).catch(() => resolve()));
  }
}


const bankAccountStore = new BankAccountStore();
export default bankAccountStore;
