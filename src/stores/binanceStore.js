import React from 'react';
import * as mobx from 'mobx';

import {
  BTCSOCKET,
  LTCSOCKET,
  ETHSOCKET,
  XRPSOCKET,
} from '../consts/websockets';

import OperationStore from './operationStore';

const {
  observable,
  action,
} = mobx;

class BinanceStore {
  @observable currencies = {
    BTC: null,
    ETH: null,
    LTC: null,
    XRP: null,
    TRY: null,
  };
  @observable interval = null;
  @observable isReady = false;

  @action sockets() {
    // BITCOIN - USDT
    const btcusdtSocket = new WebSocket(BTCSOCKET);
    btcusdtSocket.onopen = () => {};
    btcusdtSocket.onmessage = (message) => {
      this.setCurrencies('BTC', JSON.parse(message.data).c);
    };
    btcusdtSocket.onerror = () => {};
    btcusdtSocket.onclose = () => { setTimeout(() => { this.sockets(); }, 1000); };

    // ETHERIUM - USDT
    const ethusdtSocket = new WebSocket(ETHSOCKET);
    ethusdtSocket.onopen = () => {};
    ethusdtSocket.onmessage = (message) => {
      this.setCurrencies('ETH', JSON.parse(message.data).c);
    };
    ethusdtSocket.onerror = () => {};
    ethusdtSocket.onclose = () => { setTimeout(() => { this.sockets(); }, 1000); };

    // ETHERIUM - USDT
    const ltcusdtSocket = new WebSocket(LTCSOCKET);
    ltcusdtSocket.onopen = () => {};
    ltcusdtSocket.onmessage = (message) => {
      this.setCurrencies('LTC', JSON.parse(message.data).c);
    };
    ltcusdtSocket.onerror = () => {};
    ltcusdtSocket.onclose = () => { setTimeout(() => { this.sockets(); }, 1000); };

    // RIPPLE - USDT
    const xrpusdtSocket = new WebSocket(XRPSOCKET);
    xrpusdtSocket.onopen = () => {};
    xrpusdtSocket.onmessage = (message) => {
      this.setCurrencies('XRP', JSON.parse(message.data).c);
    };
    xrpusdtSocket.onerror = () => {};
    xrpusdtSocket.onclose = () => { setTimeout(() => { this.sockets(); }, 1000); };
  }

  @action openusdtryHttpRequest() {
    this.usdtryHttpRequest();
    this.interval = setInterval(() => {
      this.usdtryHttpRequest();
    }, 60000);
  }
  @action usdtryHttpRequest() {
    fetch('https://forex.1forge.com/1.0.3/quotes?pairs=USDTRY&api_key=xOwlNnXDJmjcBKsC3M84toAtkZFeAf6I', {
      method: 'GET',
    }).then(resp => resp.json())
      .then((response) => {
        this.setCurrencies('TRY', parseFloat(response[0].price));
      })
      .catch(() => {});
  }

  @action setCurrencies(currency, value) {
    const {
      BTC,
      ETH,
      LTC,
      XRP,
      TRY,
    } = this.currencies;
    if (BTC && ETH && LTC && XRP && !this.isReady) {
      this.isReady = true;
      OperationStore.calculateUnitPrice();
    }
    this.currencies[currency] = value;
  }
}

const binanceStore = new BinanceStore();
export default binanceStore;
