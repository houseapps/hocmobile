import React from 'react';
import * as mobx from 'mobx';
import API from '../helpers/api';

import UIC from './uiControllerStore';

import CryptoWithdrawForm from './forms/cryptoWithdrawForm';
import CryptoDepositForm from './forms/cryptoDepositForm';

const {
  action,
} = mobx;

class CryptoStore {
  @action cryptoWithdraw() {
    return new Promise((resolve, reject) => {
      const { isFormValidated, giveError, form } = CryptoWithdrawForm;
      const { amount, asset, walletId } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = {
          amount,
          asset,
          wallet_address: walletId,
        };
        return API.request('POST', 'payments/blockchain_withdraw/', postBody)
          .then((response) => {
            UIC.openAlertPopup('İşleminiz Başarıyla Gerçekleşti.', 0);
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup('Bir Hata Meydana Geldi.', 1);
            return reject();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }

  @action cryptoDeposit() {
    return new Promise((resolve, reject) => {
      const { isFormValidated, giveError, form } = CryptoDepositForm;
      const { amount, asset, walletAddress } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = {
          amount,
          asset,
          wallet_address: walletAddress,
        };
        return API.request('POST', 'payments/blockchain_withdraw/', postBody)
          .then((response) => {
            UIC.openAlertPopup('İşleminiz Başarıyla Gerçekleşti.', 0);
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup('Bir Hata Meydana Geldi.', 1);
            return reject();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }
}

const cryptoStore = new CryptoStore();
export default cryptoStore;
