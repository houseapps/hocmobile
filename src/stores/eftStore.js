import React from 'react';
import * as mobx from 'mobx';
import API from '../helpers/api';

import UIC from './uiControllerStore';

import EftWithdrawForm from './forms/eftWithdrawFrom';

const {
  action,
} = mobx;

class EFTStore {
  @action eftWithdraw() {
    return new Promise((resolve, reject) => {
      const { isFormValidated, giveError, form } = EftWithdrawForm;
      const { amount, account_name, iban } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = { amount: parseInt(amount, 0), account_name: { account_name }, iban: parseInt(iban, 0) };
        return API.request('POST', 'payments/denizbank_withdraw/', postBody)
          .then((response) => {
            UIC.openAlertPopup(response.detail, 0);
            EftWithdrawForm.clearForm();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            return reject();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }
}

const eftStore = new EFTStore();
export default eftStore;
