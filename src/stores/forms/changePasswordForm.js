import { observable, action, computed } from 'mobx';

class ChangePasswordForm {
    @observable form = {
      current_password: null,
      new_password: null,
      new_password_again: null,
    };

    @observable passwordVisible = {
      current_password: false,
      new_password: false,
      new_password_again: false,
    };

    @action handleUpdateForm(key, value) {
      this.form[key] = value;
    }

    @action clearForm() {
      this.form = {
        current_password: null,
        new_password: null,
        new_password_again: null,
      };
    }

    @computed get isFormValidated() {
      const {
        current_password,
        new_password,
        new_password_again,
      } = this.form;
      if (new_password === new_password_again) {
        return true;
      }
      return false;
    }


    @computed get giveError() {
      const {
        current_password,
        new_password,
        new_password_again,
      } = this.form;
      if (!current_password) {
        return 'Şuan ki parola alanı boş bırakılmamalı.';
      } else if (!new_password) {
        return 'Yeni parola alanı boş bırakılmamalı.';
      } else if (!new_password_again) {
        return 'Yeni parolanızı tekrar giriniz';
      } else if (new_password !== new_password_again) {
        return 'Yeni parolalar birbirleriyle uyuşmuyor.';
      }
      return null;
    }

    @action changePasswordVisible(key) {
      this.passwordVisible[key] = !this.passwordVisible[key];
    }

    @action formReducer() {
      this.form = {
        current_password: null,
        new_password: null,
        new_password_again: null,
      };
    }
}

export default new ChangePasswordForm();
