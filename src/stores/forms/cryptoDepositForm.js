import { observable, action, computed } from 'mobx';

class CryptoDepositForm {
  @observable form = {
    amount: 0,
    walletAddress: 'WalletID yapıştırmak için buraya basılı tutun.',
    asset: 'LTC',
  };

  cryptoList = ['BTC', 'LTC', 'ETH']

  @computed get fullName() {
    if (this.form.asset === 'BTC') return 'Bitcoin';
    if (this.form.asset === 'LTC') return 'Litecoin';
    if (this.form.asset === 'ETH') return 'Etherium';
  }

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @action clearForm() {
    this.form = {
      amount: 0,
      walletAddress: 'WalletID yapıştırmak için buraya basılı tutun.',
      asset: this.form.asset,
    };
  }

  @computed get isFormValidated() {
    const {
      amount,
    } = this.form;
    if (amount <= 0) return false;
    if (amount > 10) return false;
    if (!amount) return false;
    return true;
  }

  @computed get giveError() {
    const {
      amount,
    } = this.form;
    if (!amount) {
      return 'Lütfen bir tutar giriniz.';
    } else if (amount <= 0) {
      return "Girdiğiniz tutar 0'dan büyük olmalıdır.";
    } else if (amount > 10) {
      return "Girdiğiniz tutar 10'dan küçük olmalıdır.";
    } return null;
  }
}

export default new CryptoDepositForm();
