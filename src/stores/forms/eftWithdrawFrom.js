import React from 'react';
import { observable, action, computed } from 'mobx';

class EftWithdrawForm {
  @observable form = {
    amount: null,
    account_name: null,
    iban: null,
  };


  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @computed get isFormValidated() {
    const {
      amount,
      iban,
      account_name,
    } = this.form;
    if (amount && amount > 0 && iban && account_name) return true;
    else return false;
  }

  @computed get giveError() {
    const { amount, iban, account_name } = this.form;
    if (!amount) {
      return 'Miktar belirtmelisiniz';
    } else if (amount <= 0) {
      return 'Miktarınız sıfırdan büyük olmalıdır';
    } else if (!iban) {
      return 'IBAN girmeniz gerekmektedir';
    } else if (!account_name) {
      return 'Hesap adı girmelisiniz.';
    } else return null;
  }


  @action clearForm() {
    this.form = {
      amount: null,
      account_name: null,
      iban: null,
    };
  }
}

export default new EftWithdrawForm();
