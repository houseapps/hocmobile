import { observable, action, computed } from 'mobx';
import ImagePicker from 'react-native-image-picker';

import UserStore from '../userStore';

const options = {
  title: 'Select Avatar',
  customButtons: [
    { name: 'fb', title: 'Choose Photo from Facebook' },
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class IDCardForm {
  @observable form = {
    fcardPhoto: null,
    bcardPhoto: null,
    sPhoto: null,
  };
  @observable currentStep = 0;
  @observable isCamera = false;
  @observable displayResult = false;

  async takePicture(camera) {
    const settings = { quality: 0.1, base64: true };
    const data = await camera.takePictureAsync(settings);
    this.isCamera = false;
    this.setImage(data);
  }

  @action getDocuments() {
    if (UserStore.user.user.document) {
      const { back, front, selfy } = UserStore.user.user.document;
      this.form = {
        fcardPhoto: front,
        bcardPhoto: back,
        sPhoto: selfy,
      };
    }
  }

  @computed get isUpdate() {
    const {
      fcardPhoto,
      bcardPhoto,
      sPhoto,
    } = this.form;
    if (typeof (fcardPhoto) === 'object' &&
    typeof (bcardPhoto) === 'object' &&
    typeof (sPhoto) === 'object'
    ) return false;
    else return true;
  }

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @action setImage(data) {
    if (this.currentStep === 0) {
      this.form.fcardPhoto = data;
    } else if (this.currentStep === 1) {
      this.form.bcardPhoto = data;
    } else {
      this.form.sPhoto = data;
    }
  }
  @action launchCamera() {
    if (this.isCamera) this.camera = false;
    else this.isCamera = true;
    this.setImage(null);
  }

  @action launchGallery() {
    this.isCamera = false;
    ImagePicker.launchImageLibrary(options, (response) => {
      const { data } = response;
      if (data) this.setImage(response);
    });
  }

  @computed get getCardVerificationStatus() {
    if (UserStore.user) {
      const { document } = UserStore.user.user;
      if (document) {
        return document.status;
      } else return false;
    }
    return null;
  }

  @computed get isNextStep() {
    if (this.currentStep === 0 && this.form.fcardPhoto) {
      return true;
    } else if (this.currentStep === 1 && this.form.bcardPhoto) {
      return true;
    } else if (this.currentStep === 2 && this.form.sPhoto) {
      return true;
    } else {
      return false;
    }
  }

  @action clearForm() {
    this.form = {
      fcardPhoto: null,
      bcardPhoto: null,
      sPhoto: null,
    };
  }

  @action clearStates() {
    this.currentStep = 0;
    this.isCamera = false;
    this.displayResult = false;
  }
}

export default new IDCardForm();
