import { observable, action, computed } from 'mobx';

import moment from 'moment';

class IDInfoForm {
  @observable form = {
    tckn: null,
    birthday: null,
    name: null,
    surname: null,
  };

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }
  @action preparePostBody() {
    const {
      tckn,
      birthday,
      name,
      surname,
    } = this.form;
    const postBody = {
      tckn: parseInt(tckn, 0).toString(),
      year: moment(birthday).get('year').toString(),
      month: (moment(birthday).get('month') + 1).toString(),
      day: moment(birthday).get('date').toString(),
      name,
      surname,
    };
    return postBody;
  }
  @computed get isFormValidated() {
    const {
      tckn,
      birthday,
      name,
      surname,
    } = this.form;
    if (tckn &&
      name &&
      surname &&
      birthday
    ) return true;
    else return false;
  }

  @computed get giveError() {
    const {
      tckn,
      birthday,
      name,
      surname,
    } = this.form;
    if (!tckn) {
      return 'Kimlik No alanı boş bırakılmamalı.';
    } else if (!name) {
      return 'İsim alanı boş bırakılmamalı.';
    } else if (!surname) {
      return 'Soyisim alanı boş bırakılmamalı.';
    } else if (!birthday) {
      return 'Doğum tarihi alanı boş bırakılmamalı.';
    } else return null;
  }

  @action clearForm() {
    this.form = {
      tckn: null,
      birthday: null,
      name: null,
      surname: null,
    };
  }
}

export default new IDInfoForm();
