import { observable, action, computed } from 'mobx';
import { ValidateEmail } from '../../helpers/utils';

class LoginForm {
  @observable form = {
    email: null,
    password: null,
  };

  // request by param

  @observable passwordVisible = false;

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @action clearForm() {
    this.form = {
      email: null,
      password: null,
    };
  }

  @computed get isFormValidated() {
    const {
      email,
      password,
    } = this.form;
    if (ValidateEmail(email) &&
      password
    ) return true;
    else return false;
  }

  @computed get giveError() {
    const {
      email,
      password,
    } = this.form;
    // console.log('validate email', email);
    if (!email) {
      return 'E-mail alanı boş bırakılmamalı.';
    } else if (!ValidateEmail(email)) {
      return 'E-mail adresi geçerli değil.';
    } else if (!password) {
      return 'Parola alanı boş bırakılmamalı.';
    } else return null;
  }

  @action changePasswordVisible() {
    this.passwordVisible = !this.passwordVisible;
  }

  @action formReducer() {
    this.form = {
      email: null,
      password: null,
    };
  }
}

export default new LoginForm();
