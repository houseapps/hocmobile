import { observable, action, computed } from 'mobx';
import { PriceFormat } from '../../helpers/utils';

import OperationStore from '../operationStore';
import UserStore from '../userStore';


class OperationForm {
  @observable baseAmount = 0;

  @action changeBaseAmount(value) {
    this.baseAmount = value;
  }

  @computed get calculateQuoteAmount() {
    const { shownUnitPrice } = OperationStore;
    return PriceFormat(this.baseAmount * shownUnitPrice);
  }

  @computed get getBaseBalance() {
    const { base } = OperationStore;
    const { user } = UserStore;
    if (user) {
      const { balances } = user.user;
      const balance = balances.find(b => b.asset === base);
      if (!balance) return 0;
      return parseFloat(balance.final_balance).toString();
    } else return 0;
  }

  @computed get getQuoteBalance() {
    const { quote } = OperationStore;
    const { user } = UserStore;
    if (user) {
      const { balances } = user.user;
      const balance = balances.find(b => b.asset === quote);
      if (!balance) return 0;
      return parseFloat(balance.final_balance).toString();
    } else return 0;
  }
}

export default new OperationForm();
