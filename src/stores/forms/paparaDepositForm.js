import { observable, action, computed } from 'mobx';
import React from 'react';
import { Alert } from 'react-native';
import UIC from '../uiControllerStore';

class PaparaDepositForm {
  @observable form = {
    amount: null,
    paymentUrl: null,
  };

  @observable isLoaded = false;

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @action onWebViewMessage(data) {
    if (data.nativeEvent.data === 'success') {
      UIC.closePaparaWebViewModal();
      UIC.openAlertPopup('Para yatırma işleminiz başarıyla gerçekleşti.', 0);
    } else if (data.nativeEvent.data === 'failure') {
      UIC.closePaparaWebViewModal();
      UIC.openAlertPopup('Para yatırma işleminiz başarısız oldu.', 0);
    }
  }

  @computed get isFormValidated() {
    const {
      amount,
    } = this.form;
    if (amount && amount > 0) return true;
    else return false;
  }

  @computed get giveError() {
    const { amount } = this.form;
    if (!amount) {
      return 'Miktar belirtmelisiniz';
    } else if (amount <= 0) {
      return 'Miktarınız sıfırdan büyük olmalıdır';
    } else return null;
  }


  @action formReducer() {
    this.form = {
      amount: null,
      paymentUrl: null,
    };
  }
}

export default new PaparaDepositForm();
