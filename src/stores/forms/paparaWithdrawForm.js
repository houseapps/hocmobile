import React from 'react';
import { observable, action, computed } from 'mobx';

class PaparaDepositForm {
  @observable form = {
    amount: null,
    accountNumber: null,
  };


  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @computed get isFormValidated() {
    const {
      amount,
      accountNumber,
    } = this.form;
    if (amount && amount > 0 && accountNumber) return true;
    else return false;
  }

  @computed get giveError() {
    const { amount, accountNumber } = this.form;
    if (!amount) {
      return 'Miktar belirtmelisiniz';
    } else if (amount <= 0) {
      return 'Miktarınız sıfırdan büyük olmalıdır';
    } else if (!accountNumber) {
      return 'Hesap numaranızı girmelisiniz.';
    } else return null;
  }


  @action clearForm() {
    this.form = {
      amount: null,
      accountNumber: null,
    };
  }
}

export default new PaparaDepositForm();
