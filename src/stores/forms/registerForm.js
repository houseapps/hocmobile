import { observable, action, computed } from 'mobx';
import { ValidateEmail } from '../../helpers/utils';

class RegisterForm {
  @observable form = {
    name: null,
    surname: null,
    email: null,
    password: null,
    passwordAgain: null,
    ks: false,
    kvkk: false,
  };
  @observable passwordVisible = false;
  @observable passwordAgainVisible = false;

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }

  @computed get isFormValidated() {
    const {
      name,
      surname,
      email,
      password,
      passwordAgain,
      ks,
      kvkk,
    } = this.form;
    if (ValidateEmail(email) &&
      name &&
      surname &&
      email &&
      password &&
      passwordAgain &&
      password === passwordAgain &&
      ks &&
      kvkk
    ) return true;
    else return false;
  }

  @computed get giveError() {
    const {
      name,
      surname,
      email,
      password,
      passwordAgain,
      ks,
      kvkk,
    } = this.form;
    if (!name) {
      return 'İsim alanı boş bırakılmamalı.';
    } else if (!surname) {
      return 'Soyisim alanı boş bırakılmamalı.';
    } else if (!email) {
      return 'E-mail alanı boş bırakılmamalı.';
    } else if (!ValidateEmail(email)) {
      return 'E-mail adresiniz geçerli değil.';
    } else if (!password) {
      return 'Şifre alanı boş bırakılmamalı.';
    } else if (!passwordAgain) {
      return 'Şifre tekrarı alanı boş bırakılmamalı.';
    } else if (password !== passwordAgain) {
      return 'Şifreleriniz eşleşmeli.';
    } else if (!ks) {
      return 'Kullanıcı sözleşmesini kabul etmelisiniz.';
    } else if (!kvkk) {
      return 'Kişisel verilerin korunması kanununu kabul etmelisiniz..';
    } else return null;
  }
  @action changePasswordVisible(type) {
    const { passwordVisible, passwordAgainVisible } = this;
    if (type === 0) this.passwordVisible = !passwordVisible;
    else this.passwordAgainVisible = !passwordAgainVisible;
  }

  @action changeKS() {
    this.form.ks = !this.form.ks;
  }

  @action changeKVKK() {
    this.form.kvkk = !this.form.kvkk;
  }

  @action formReducer() {
    this.form = {
      name: null,
      surname: null,
      email: null,
      password: null,
      password_again: null,
      ks: false,
      kvkk: false,
    };
  }
}

export default new RegisterForm();
