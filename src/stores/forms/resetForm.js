import { observable, action, computed } from 'mobx';
import { ValidateEmail } from '../../helpers/utils';

class ResetForm {
  @observable form = {
    email: null,
  };

  @action handleUpdateForm(key, value) {
    this.form[key] = value;
  }
  @computed get isFormValidated() {
    const { email } = this.form;
    if (ValidateEmail(email)) return true;
    else return false;
  }

  @action clearForm() {
    this.form = {
      email: null,
    };
  }

  @computed get giveError() {
    const { email } = this.form;
    if (!email) {
      return 'E-Posta alanı boş bırakılmamalı.';
    }
    return null;
  }
}
export default new ResetForm();
