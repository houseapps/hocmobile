import { observable, action, computed } from 'mobx';
import UIC from '../uiControllerStore';

class SmsForm {
  @observable phone = '';
  @observable code = '';
  @observable step = 1;

  @action handleUpdateForm(key, value) {
    this[key] = value;
  }

  @computed get isFormValidated() {
    const { phone } = this;
    if (phone.length === 14) return true;
    else return false;
  }

  @action timeUp() {
    UIC.stopTimer();
    UIC.openTimeUpModal();
  }
}

export default new SmsForm();
