import { action, computed } from 'mobx';

import BinanceStore from '../binanceStore';
import TransactionStore from '../transactionStore';

class TransactionForm {
  @action totalBalanceAsUSD(assetType, amount) {
    const { currencies } = BinanceStore;
    return (amount * currencies[assetType]).toFixed(2);
  }

  @computed get transactionsByValue() {
    const { transactions } = TransactionStore;
    return transactions.filter(transaction => parseFloat(transaction.final_balance, 8) !== 0.00000000);
  }

  @computed get getTry() {
    const { transactions } = TransactionStore;
    return transactions.map((transaction) => {
      if (transaction.asset === 'TRY') return parseFloat(transaction.final_balance).toFixed(2);
    });
  }
}

export default new TransactionForm();
