import React from 'react';
import { Platform } from 'react-native';
import * as mobx from 'mobx';

const {
  observable,
  action,
} = mobx;

class NavigationStore {
  @observable navigator = null;
  @action route(screen, navBarHidden = true, props = {}, nav = null, title = '', tabBarHidden = false) {
    const { navigator } = this;
    if (nav) {
      nav.push({
        screen,
        title,
        passProps: { props },
        navigatorStyle: {
          navBarHidden,
          tabBarHidden,
        },
      });
    } else if (navigator) {
      navigator.push({
        screen,
        title,
        passProps: { props },
        navigatorStyle: {
          navBarHidden,
          tabBarHidden,
        },
      });
    }
  }
  @action pop(nav = null) {
    const { navigator } = this;
    if (nav) nav.pop();
    else navigator.pop();
  }
}

const navigationStore = new NavigationStore();
export default navigationStore;
