import React from 'react';
import * as mobx from 'mobx';
import API from '../helpers/api';

import UIC from './uiControllerStore';
import BinanceStore from './binanceStore';
import UserStore from './userStore';

const {
  observable,
  action,
  computed,
} = mobx;

class OperationStore {
  @observable binanceStore = null;
  @observable productList = [];
  @observable base = null;
  @observable quote = null;
  @observable productsModalList = [];
  @observable selectedType = null;
  @observable calculatedUnitPrice = 0;
  @observable shownUnitPrice = 0;

  // Products long names, These will be receive from service with products
  @observable productsLongNames = {
    BTC: 'Bitcoin',
    LTC: 'Litecoin',
    ETH: 'Etherium',
    XRP: 'Ripple',
    USDT: 'Tether',
    TRY: 'Turkish Lira',
  }

  @computed get getQuotes() {
    const { productList, base } = this;
    return productList.filter(product => product.asset === base);
  }

  @computed get getBases() {
    const { productList, quote } = this;
    return productList.filter(product => product.asset === quote);
  }

  @computed get calculate() {
    return 0;
  }

  @action changeBase(base) {
    this.base = base;
    console.log('1111 base', base);
    const quotes = this.getQuotes.map((item) => { return item.asset; });
    console.log('1111 quotes', quotes);
    console.log('1111 this.quote', this.quote);
    if (quotes.includes(this.quote)) {
      this.quote = this.productList.filter(quote => quote.asset !== base)[0].asset
      console.log('1111 quote', this.quote);
    }
  }

  @action changeQuote(quote) {
    this.quote = quote;
    const bases = this.getBases.map((item) => { return item.asset; });
  
    if (bases.includes(this.base)) {
      this.base = this.productList.filter(base => base.asset !== quote)[0].asset
    }
  }

  @action getProductsModalList(type, asset) {
    const as = type === 'base' ? this.quote : this.base; 
    this.selectedType = type;
    // const list = type === 'base' ? this.productList : this.getQuotes;
    this.productsModalList = this.productList.filter(product => product.asset !== asset)
    console.log('222productsModalList', this.productsModalList);
  }

  @action selectCurrency(item) {
    console.log('item', item)
    if (this.selectedType === 'base') this.changeBase(item.asset);
    else this.changeQuote(item.asset)
    UserStore.timer = 0.99;
    this.getDisplayNewUnitPrice(true);
    UIC.closeProductsModal();
  }
  @action getDisplayNewUnitPrice(force) {
    if (this.shownUnitPrice === 0 || force) {
      this.shownUnitPrice = this.calculateUnitPrice();
      this.changePrice = false;
    }
  }
  @action calculateUnitPrice() {
    const { currencies } = BinanceStore;
    let price = 0;
    if (this.base && currencies[this.base]) {
      if (this.quote === 'TRY') {
        price = currencies[this.base] * this.currencies.TRY;
      } else if (this.quote === 'USDT') {
        price = parseFloat(currencies[this.base]);
      } else {
        price = currencies[this.base] / currencies[this.quote];
      }
      if (this.quote === 'TRY' || this.quote === 'USDT') {
        this.calculatedUnitPrice = price.toFixed(2);
        this.shownUnitPrice = price.toFixed(2);
        return price.toFixed(2);
      } else {
        this.calculatedUnitPrice = price.toFixed(6);
        this.shownUnitPrice = price.toFixed(6);
        return price.toFixed(6);
      }
    } else return price;
  }
}

const operationStore = new OperationStore();
export default operationStore;
