import React from 'react';
import * as mobx from 'mobx';

import API from '../helpers/api';
import {
  PAPARA_PAYMENT_CREATE,
  PAPARA_PAYMENT_WITHDRAW,
} from '../helpers/endpoints';

import UIC from './uiControllerStore';

import PaparaDepositForm from './forms/paparaDepositForm';
import PaparaWithdrawForm from './forms/paparaWithdrawForm';

const {
  action,
} = mobx;

class PaparaStore {
  @action paparaDeposit() {
    return new Promise((resolve, reject) => {
      const { isFormValidated, giveError, form } = PaparaDepositForm;
      const { amount } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = { amount };
        return API.request('POST', PAPARA_PAYMENT_CREATE, postBody)
          .then((response) => {
            PaparaDepositForm.handleUpdateForm('paymentUrl', response.payment_url);
            UIC.openPaparaWebViewModal();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            return reject();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }

  @action paparaWithdraw() {
    return new Promise((resolve, reject) => {
      const { isFormValidated, giveError, form } = PaparaWithdrawForm;
      const { amount, accountNumber } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = { amount: parseInt(amount, 0), account_number: accountNumber.toString() };
        return API.request('POST', PAPARA_PAYMENT_WITHDRAW, postBody)
          .then((response) => {
            UIC.openAlertPopup(response.detail, 0);
            PaparaWithdrawForm.clearForm();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            return reject();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }
}

const paparaStore = new PaparaStore();
export default paparaStore;
