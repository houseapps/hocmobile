import React from 'react';
import * as mobx from 'mobx';
import API from '../helpers/api';
import { PRODUCT_LIST } from '../helpers/endpoints';
import OperationStore from './operationStore';

const {
  action,
} = mobx;

class SystemStore {
  @action fetchProductList() {
    return API.request('GET', PRODUCT_LIST)
      .then((resp) => {
        OperationStore.productList = resp.products;
        OperationStore.base = resp.products[0].asset;
        OperationStore.quote = resp.products[1].asset;
        OperationStore.calculateUnitPrice();
      }).catch((err) => {console.log('ddd',err)});
  }
}

const systemStore = new SystemStore();
export default systemStore;
