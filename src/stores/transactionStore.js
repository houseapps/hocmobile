import React from 'react';
import * as mobx from 'mobx';

import { TRANSACTION_ASSETS_LIST, ASSET_HISTORY } from '../helpers/endpoints';
import API from '../helpers/api';

const {
  observable,
  action,
} = mobx;

class TransactionStore {
  @observable transactions = [];
  @observable transactionDetail = [];
  @observable selectedTransaction = [];

  @action fetchTransactionAssets() {
    return new Promise((resolve, reject) => API.request('GET', TRANSACTION_ASSETS_LIST)
      .then((response) => {
        this.transactions = response.detail;
        return resolve();
      }).catch(() => resolve()));
  }

  @action fetchAssetDetail(assetType) {
    this.selectedTransaction = assetType;
    return new Promise((resolve, reject) => API.request('GET', `${ASSET_HISTORY}${assetType}`)
      .then((response) => {
        this.transactionDetail = response.detail;
        return resolve();
      }).catch(() => resolve()));
  }
}


const transactionStore = new TransactionStore();
export default transactionStore;
