import React from 'react';
import * as mobx from 'mobx';

import OperationStore from './operationStore';

const {
  observable,
  action,
} = mobx;

class UIControllerStore {
  @observable currentlyVisibleScreen = null;
  @observable indicator = false;
  @observable productsModal = false;
  @observable cryptoModal = false;
  @observable timer = false;
  @observable timeUp = false;
  @observable alertState = {
    isVisible: false,
    message: null,
    type: 0,
  }
  @observable dateTimePicker = false;
  @observable paparaWebView = false;
  // 0: success, 1: error
  @action closeIndicatorPopup() {
    this.indicator = false;
  }
  @action openIndicatorPopup() {
    this.indicator = true;
  }
  @action openAlertPopup(message = null, type = 0, callback = () => {}) {
    this.alertState = {
      isVisible: true,
      message,
      type,
      callback,
    };
  }
  @action closeAlertPopup() {
    this.alertState = {
      isVisible: false,
      message: null,
      type: 0,
    };
  }

  @action openProductsModal(type, asset) {
    OperationStore.getProductsModalList(type, asset);
    this.productsModal = true;
  }

  @action closeProductsModal() {
    this.productsModal = false;
  }

  @action openDateTimePicker() {
    this.dateTimePicker = true;
  }

  @action closeDateTimePicker() {
    this.dateTimePicker = false;
  }

  @action startTimer() {
    this.timer = true;
  }

  @action stopTimer() {
    this.timer = false;
  }

  @action openTimeUpModal() {
    // this.timer = false;
    this.timeUp = true;
  }

  @action closeTimeUpModal() {
    // this.timer = true;
    this.timeUp = false;
  }

  @action openPaparaWebViewModal() {
    this.paparaWebView = true;
  }

  @action closePaparaWebViewModal() {
    this.paparaWebView = false;
  }

  @action setCurrentlyVisibleScreen(screen) {
    this.currentlyVisibleScreen = screen;
  }

  @action isActiveScreen(screen) {
    return this.currentlyVisibleScreen === screen;
  }

  @action openCryptoModal() {
    this.cryptoModal = true;
  }

  @action closeCryptoModal() {
    this.cryptoModal = false;
  }
}

const uIControllerStore = new UIControllerStore();
export default uIControllerStore;
