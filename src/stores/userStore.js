import React from 'react';
import { AsyncStorage } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as mobx from 'mobx';
import moment from 'moment';

import OneSignal from '../packages/react-native-onesignal';

import * as Router from '../helpers/router';
import API from '../helpers/api';
import {
  PROFILE,
  REGISTER,
  LOGIN,
  RESET_PASSWORD,
  CHANGE_PASSWORD,
  IDENTITY_VERIFY,
  ID_CARD_SAVE,
  PHONE_VERIFY,
  SMS_VERIFY,
} from '../helpers/endpoints';


import ChangePasswordForm from './forms/changePasswordForm';
import RegisterForm from './forms/registerForm';
import IDInfoForm from './forms/idInfoForm';
import IDCardForm from './forms/idCardForm';
import LoginForm from './forms/loginForm';
import ResetForm from './forms/resetForm';
import SmsForm from './forms/smsForm';

import NavigationStore from './navigationStore';
import UIC from './uiControllerStore';

const {
  observable,
  action,
} = mobx;

class UserStore {
  @observable user = null;
  @observable timer = 0;

  @action fetchProfile() {
    return new Promise((resolve, reject) => API.request('GET', PROFILE)
      .then((response) => {
        this.setUser(response, true);
        return resolve();
      }).catch(() => resolve()));
  }

  @action register() {
    return new Promise((resolve, reject) => {
      const { form, isFormValidated, giveError } = RegisterForm;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        OneSignal.getPermissionSubscriptionState(async ({ pushToken, userId }) => {
          const {
            name,
            surname,
            email,
            password,
            passwordAgain,
            ks,
            kvkk,
          } = form;
          const postBody = {
            user: {
              name,
              surname,
              email,
              password,
              password_again: passwordAgain,
            },
            device: {
              device_id: DeviceInfo.getUniqueID(),
              device_os: DeviceInfo.getSystemName(),
            },
            agreements: {
              user_agreement: ks,
              kvkk,
            },
          };
          if (pushToken) postBody.device.push_token = pushToken;
          if (userId) postBody.device.onesignal_token = userId;
          return API.request('POST', REGISTER, postBody)
            .then((resp) => {
              UIC.openAlertPopup(resp.detail, 0, () => NavigationStore.pop());
              RegisterForm.formReducer();
              return resolve();
            }).catch((err) => {
              UIC.openAlertPopup(err.detail, 1);
              return resolve();
            });
        });
      } else {
        UIC.openAlertPopup(giveError, 1);
        return resolve();
      }
    });
  }

  @action login() {
    return new Promise((resolve, reject) => {
      const { form, isFormValidated, giveError } = LoginForm;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        OneSignal.getPermissionSubscriptionState(async ({ pushToken, userId }) => {
          const {
            email,
            password,
          } = form;
          const postBody = {
            email,
            password,
            device_id: DeviceInfo.getUniqueID(),
            device_os: DeviceInfo.getSystemName(),
          };
          if (pushToken) postBody.push_token = pushToken;
          if (userId) postBody.onesignal_token = userId;
          return API.request('POST', LOGIN, postBody)
            .then((response) => {
              if (!response.user.id_verified) {
                NavigationStore.route('IDInfoVerificationScreen');
              } else if (!response.user.phone_number_verified) {
                NavigationStore.route('SmsVerificationScreen');
              } else Router.switchToTabBased();
              this.setUser(response);
              UIC.closeIndicatorPopup();
              return resolve();
            }).catch((error) => {
              UIC.openAlertPopup(error.detail, 1);
              return resolve();
            });
        });
      } else {
        UIC.openAlertPopup(giveError, 1);
      }
    });
  }

  @action reset() {
    return new Promise((resolve, reject) => {
      const { form, isFormValidated, giveError } = ResetForm;
      const { email } = form;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = { email };
        return API.request('POST', RESET_PASSWORD, postBody)
          .then((response) => {
            UIC.openAlertPopup(response.detail, 0);
            ResetForm.clearForm();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            UIC.closeIndicatorPopup();
            return reject();
          });
      } else {
        ResetForm.clearForm();
        UIC.openAlertPopup(giveError, 1);
        return reject();
      }
    });
  }

  @action changePassword() {
    return new Promise((resolve, reject) => {
      const { form, isFormValidated, giveError } = ChangePasswordForm;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const { current_password, new_password, new_password_again } = form;
        const postBody = {
          current_password,
          new_password,
          new_password_again,
        };
        return API.request('PUT', CHANGE_PASSWORD, postBody)
          .then((response) => {
            UIC.openAlertPopup(response.detail, 0);
            ChangePasswordForm.clearForm();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            return resolve();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
        return resolve();
      }
    });
  }

  @action verifyIDInfo() {
    return new Promise((resolve, reject) => {
      const { form, isFormValidated, giveError } = IDInfoForm;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const {
          tckn,
          birthday = moment(birthday).utc(false),
          name,
          surname,
        } = form;

        const postBody = {
          tckn: parseInt(tckn, 0).toString(),
          year: moment(birthday).get('year').toString(),
          month: (moment(birthday).get('month') + 1).toString(),
          day: moment(birthday).get('date').toString(),
          name,
          surname,
        };
        return API.request('POST', IDENTITY_VERIFY, postBody)
          .then((response) => {
            this.setUser(response.user, true);
            if (!this.user.phone_number_verified) {
              NavigationStore.route('SmsVerificationScreen');
            } else {
              Router.switchToTabBased();
            }
            UIC.closeIndicatorPopup();
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            UIC.closeIndicatorPopup();
            return resolve();
          });
      } else {
        UIC.openAlertPopup(giveError, 1);
        return resolve();
      }
    });
  }

  @action verifyIDCard() {
    return new Promise((resolve, reject) => {
      UIC.openIndicatorPopup();

      const { form } = IDCardForm;

      const {
        fcardPhoto,
        bcardPhoto,
        sPhoto,
      } = form;

      const postBody = {};

      const parsing = (obj) => {
        const { data, base64 } = obj;
        if (data) return data;
        else return base64;
      };

      if (typeof (fcardPhoto) === 'object') postBody.front = `data:image/jpeg;base64,${parsing(fcardPhoto)}`;
      if (typeof (bcardPhoto) === 'object') postBody.back = `data:image/jpeg;base64,${parsing(bcardPhoto)}`;
      if (typeof (sPhoto) === 'object') postBody.selfy = `data:image/jpeg;base64,${parsing(sPhoto)}`;

      return API.request('POST', ID_CARD_SAVE, postBody)
        .then((response) => {
          this.setUser(response.user, true);
          UIC.closeIndicatorPopup();
          IDCardForm.clearForm();
          return resolve();
        }).catch((error) => {
          UIC.closeIndicatorPopup();
          return reject();
        });
    });
  }

  @action sendCode() {
    return new Promise((resolve, reject) => {
      const { phone, isFormValidated } = SmsForm;
      if (isFormValidated) {
        UIC.openIndicatorPopup();
        const postBody = { phone: phone.replace(/[\(\)\-\s]+/g, '') };
        return API.request('POST', PHONE_VERIFY, postBody)
          .then((response) => {
            this.setUser(response.user, true);
            UIC.closeTimeUpModal();
            UIC.closeIndicatorPopup();
            SmsForm.step = 2;
            setTimeout(() => {
              UIC.startTimer();
            }, 500);
            return resolve();
          }).catch((error) => {
            UIC.openAlertPopup(error.detail, 1);
            UIC.closeIndicatorPopup();
            return reject();
          });
      } else return reject();
    });
  }

  @action verifyCode() {
    return new Promise((resolve, reject) => {
      const { code } = SmsForm;
      UIC.openIndicatorPopup();
      const postBody = { sms: code };
      return API.request('POST', SMS_VERIFY, postBody)
        .then((response) => {
          this.setUser(response.user, true);
          UIC.openAlertPopup(
            'Telefon doğrulamanız başarıyla gerçekleşti',
            0,
            () => Router.switchToTabBased(),
          );
          return resolve();
        }).catch((error) => {
          UIC.openAlertPopup(error.detail, 1);
          return reject();
        });
    });
  }

  @action setUser(user, update = false) {
    if (update) {
      this.setUser({
        token: this.user.token,
        user,
      });
    } else {
      this.user = user;
    }
    AsyncStorage.setItem('user', JSON.stringify(this.user));
  }

  @action getUser() {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('user', (err, result) => {
        if (result) {
          resolve();
          this.user = JSON.parse(result);
          console.log('user', JSON.parse(result))
        } else {
          reject();
        }
      });
    });
  }

  @action logout() {
    AsyncStorage.removeItem('user').then(() => {
      this.user = null;
      Router.startApplication('LoginScreen');
    }).catch(() => {});
  }
}

const userStore = new UserStore();
export default userStore;
