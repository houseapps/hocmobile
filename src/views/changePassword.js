import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react/native';

import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import ChangePasswordForm from '../stores/forms/changePasswordForm';
import NavigationStore from '../stores/navigationStore';
import UserStore from '../stores/userStore';

import { leftButtons, rightButtons, navigatorStyle } from '../consts/navigatorStyle';

@observer
export default class ChangePassword extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  renderChangeButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="Parolayı Değiştir"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => UserStore.changePassword() }
      />
    );
  }
  renderForm() {
    const { passwordVisible } = ChangePasswordForm;
    const { current_password, new_password, new_password_again } = ChangePasswordForm.form;
    return (
      <View style={ styles.formContainer }>
        <View style={ styles.btnGroup }>
          <TextInput
            label="Mevcut Parola"
            isVisible={ passwordVisible.current_password }
            onChangeText={ value => ChangePasswordForm.handleUpdateForm('current_password', value) }
            onPressEye={ () => ChangePasswordForm.changePasswordVisible('current_password') }
            eyeState={ passwordVisible.current_password }
            eye
            value={ current_password }
          />
          <TextInput
            label="Yeni Parola"
            isVisible={ passwordVisible.new_password }
            onChangeText={ value => ChangePasswordForm.handleUpdateForm('new_password', value) }
            onPressEye={ () => ChangePasswordForm.changePasswordVisible('new_password') }
            eyeState={ passwordVisible.new_password }
            eye
            value={ new_password }
          />
          <TextInput
            label="Yeni Parola Doğrulama"
            isVisible={ passwordVisible.new_password_again }
            onChangeText={ value => ChangePasswordForm.handleUpdateForm('new_password_again', value) }
            onPressEye={ () => ChangePasswordForm.changePasswordVisible('new_password_again') }
            eyeState={ passwordVisible.new_password_again }
            eye
            value={ new_password_again }
          />
        </View>
      </View>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <Indicator />
          {this.renderForm()}
          <View style={ styles.bottomSpace }>
            <View style={ styles.buttonContainer }>
              {this.renderChangeButton()}
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  formContainer: {
    flexDirection: 'column',
    flex: 3,
    justifyContent: 'center',
  },
  btnGroup: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginTop: Statics.size(50),
  },
  bottomSpace: {
    flex: 3,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonContainer: {
    marginBottom: Statics.size(30),
  },
});

