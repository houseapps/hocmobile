
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Clipboard,
} from 'react-native';
import { observer } from 'mobx-react/native';

import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import CryptoStore from '../stores/cryptoStore';
import CryptoWithdrawForm from '../stores/forms/cryptoWithdrawForm';

import { leftButtons, rightButtons, navigatorStyle } from '../consts/navigatorStyle';

@observer
export default class CryptoWitdraw extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  render() {
    const { amount, walletId, asset } = CryptoWithdrawForm.form;
    return (
      <View style={ styles.container }>
        <Indicator />
        <View style={ styles.screenTitle }>
          <Image
            uri={ asset.toLowerCase() }
            height={ 36 }
            width={ 36 }
          />
          <Text style={ styles.title }> {CryptoWithdrawForm.fullName} Çek</Text>
        </View>
        <View style={ styles.content }>
          <View style={ styles.sectionHeader }>
            <Text style={ styles.sectionTitle }>Hesap Bakiyeniz</Text>
          </View>
          <View style={ styles.amount }>
            <Text style={ styles.cryptoAmount }>2.131</Text>
            <Text style={ styles.cryptoAsset }> BTC</Text>
          </View>
          <View style={ styles.wallet }>
            <TouchableOpacity
              style={ styles.walletTouchableContainer }
              onLongPress={ () => {
                Clipboard.getString().then((content) => {
                  CryptoWithdrawForm.handleUpdateForm('walletId', content);
                });
              } }
            >
              <View style={ styles.walletContainer }>
                <Text style={ styles.walletIdText }>Wallet ID</Text>
                <Text style={ styles.walletId }>{ walletId }</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={ styles.qrContainer }
              onPress={ () => NavigationStore.route('WithdrawQRScannerScreen', false, {}, this.props.navigator, 'QR Kod', true) }
            >

              <Image uri="qr" height={ 42 } width={ 42 } />
            </TouchableOpacity>
          </View>
          <View style={ styles.inputContainer }>
            <View style={ styles.textInputStyle }>
              <TextInput
                onChangeText={ value => CryptoWithdrawForm.handleUpdateForm('amount', value) }
                value={ amount }
                label="Tutar"
              />
            </View>
          </View>
        </View>
        <View style={ styles.footer }>
          <Button
            height={ Statics.size(56) }
            width={ Statics.WIDTH * 0.829 }
            label="ÇEK"
            labelColor="white"
            backgroundColor={ Colors.navyBlue }
            fontSize={ Statics.size(21) }
            onPress={ () => CryptoStore.cryptoWithdraw() }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    flexDirection: 'column',
    margin: Statics.size(15),
  },
  screenTitle: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: Statics.size(24),
    fontFamily: Fonts.PRegular,
    color: Colors.navyBlue,
    marginLeft: 5,
  },
  content: {
    flex: 6,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sectionHeader: {
    paddingTop: Statics.size(10),
    paddingBottom: Statics.size(10),
    fontSize: Statics.size(18),
    fontFamily: Fonts.PRegular,
    color: Colors.navyBlue,
    borderBottomColor: Colors.borderColor,
    borderBottomWidth: 0.5,
  },
  sectionTitle: {
    fontSize: Statics.size(18),
    fontFamily: Fonts.PRegular,
    color: Colors.navyBlue,
  },
  amount: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wallet: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginBottom: Statics.size(30),
    width: Statics.WIDTH * 0.829,
  },
  walletContainer: {
    flex: 1,
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    backgroundColor: 'white',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 6,
    padding: Statics.size(10),
  },
  inputContainer: {
    marginTop: Statics.size(7),
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  walletTouchableContainer: {
    flex: 14,
  },
  qrContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  walletIdText: {
    opacity: 0.5,
    fontSize: Statics.size(14),
    fontFamily: Fonts.PRegular,
    color: Colors.titleGray,
  },
  walletId: {
    fontSize: Statics.size(20),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  textInputStyle: {
    alignSelf: 'center',
    flex: 1,
  },
  cryptoAmount: {
    fontSize: Statics.size(40),
    color: Colors.navyBlue,
  },
  cryptoAsset: {
    fontSize: Statics.size(19),
    color: Colors.navyBlue,
    marginTop: Statics.size(10),
  },
});

