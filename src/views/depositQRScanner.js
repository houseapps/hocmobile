import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Alert,
} from 'react-native';
import { QRscanner } from 'react-native-qr-scanner';
import { observer } from 'mobx-react/native';

import CryptoDepositForm from '../stores/forms/cryptoDepositForm';

import * as Statics from '../helpers/statics';

import { navigatorStyle, leftButtons } from '../consts/navigatorStyle';

@observer
export default class DepositQrScanner extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  onRead = (res) => {
    Alert.alert(JSON.stringify(res.data));
    CryptoDepositForm.handleUpdateForm('walletAddress', res.data);
    const { navigator } = this.props;
    navigator.pop();
  }
  render() {
    return (
      <View style={ styles.container }>
        <QRscanner
          onRead={ this.onRead }
          hintText=""
          finderY={ 50 }
          rectHeight={ Statics.size(300) }
          rectWidth={ Statics.size(300) }
          zoom={ 0.001 }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
});
