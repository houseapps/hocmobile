
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
} from 'react-native';

import { observer } from 'mobx-react/native';

import Indicator from '../components/indicator';
import BankInfoItem from '../components/bankInfoItem';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import BankAccountStore from '../stores/bankAccountStore';

import { navigatorStyle, leftButtons, rightButtons } from '../consts/navigatorStyle';

@observer
export default class EFTDeposit extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
    BankAccountStore.fetchBankAccounts();
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  renderItem(account) {
    return (
      <BankInfoItem { ...account } />
    );
  }
  render() {
    const { bankAccountList } = BankAccountStore;
    const { code } = BankAccountStore;
    return (
      <View style={ styles.container }>
        <Indicator />
        <View style={ styles.infoContainer }>
          <View style={ styles.paymentView }>
            <Text style={ styles.paymentType }>EFT / HAVALE </Text>
            <Text style={ styles.paymentTypeText }> ile</Text>
          </View>
          <Text style={ styles.infoText }>Tek kullanımlık referans kodu aşagıdaki gibidir. Ödeme yaparken açıklama alanına bu kodu giriniz.</Text>
          <View style={ styles.content }>
            <View style={ styles.row }>
              <Text style={ styles.title }>Referans Kodu:</Text>
              <Text selectable={ true } style={ styles.accountInfo }>{ code }</Text>
            </View>
            <FlatList
              style={ styles.flatlist }
              data={ bankAccountList }
              renderItem={ ({ item }) => this.renderItem(item) }
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
    width: Statics.WIDTH,
  },
  paymentType: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  paymentTypeText: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  infoText: {
    fontSize: Statics.size(19),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginTop: Statics.size(20),
  },
  paymentView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoContainer: {
    flex: 1,
    padding: Statics.size(20),
    width: Statics.WIDTH,
  },
  content: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: Statics.size(30),
  },
  row: {
    flexDirection: 'row',
    marginBottom: Statics.size(5),
  },
  title: {
    fontSize: Statics.size(18),
    fontFamily: Fonts.PBold,
    color: Colors.navyBlue,
    marginRight: Statics.size(10),
    marginTop: Statics.size(2),
  },
  accountInfo: {
    marginBottom: Statics.size(20),
    fontSize: Statics.size(20),
    color: Colors.green,
    fontFamily: Fonts.PBold,
  },
  flatlist: {
    width: '100%',
  },
});
