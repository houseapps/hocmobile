import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import { observer } from 'mobx-react/native';

import CurrenciesModal from '../components/currenciesModal';
import AnimatedNumber from '../components/animatedNumber';
import ProgressBar from '../components/progressbar';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import OperationStore from '../stores/operationStore';
import BinanceStore from '../stores/binanceStore';
import SystemStore from '../stores/systemStore';
import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';

import OperationForm from '../stores/forms/operationForm';

import { navigatorStyle, rightButtons } from '../consts/navigatorStyle';

@observer
export default class Home extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UIC.setCurrentlyVisibleScreen('HomeScreen');
  }
  componentDidMount() {
    SystemStore.fetchProductList();
    UserStore.fetchProfile();
    NavigationStore.navigator = this.props.navigator;
  }
  renderBackgroundColor() {
    return (
      <View style={ styles.blueContainer }>
        <View style={ styles.unitPriceView }>
          <View style={ styles.rowSection }>
            <Text style={ styles.unitPriceBaseTypeText }>{OperationStore.base} / </Text>
            <Text style={ styles.unitPriceQuoteTypeText }>{OperationStore.quote}</Text>
          </View>
          {OperationStore.shownUnitPrice > 0 &&
            <AnimatedNumber
              style={ styles.unitPriceQuotePriceText }
              value={ OperationStore.shownUnitPrice }
              formatter={ 6 }
            />
          }
        </View>
      </View>
    );
  }
  renderBaseUnit() {
    const { getBaseBalance, baseAmount } = OperationForm;
    const { base } = OperationStore;
    return (
      <View style={ styles.baseUnitContainer }>
        <View style={ styles.baseUnitInputView }>
          <TextInput
            keyboardType="numeric"
            underlineColorAndroid="transparent"
            style={ styles.baseUnitInput }
            onChangeText={ (value) => { OperationForm.changeBaseAmount(value); } }
            value={ baseAmount }
          />
        </View>
        <View style={ styles.baseUnitView }>
          <TouchableOpacity
            onPress={ () => UIC.openProductsModal('base', base) }
            style={ styles.baseUnitTouchableLabel }
          >
            <Text style={ styles.baseUnitLabel }>{ base }</Text>
            <Image
              uri="triangleGray"
              height={ Statics.size(11) }
              width={ Statics.size(14) }
            />
          </TouchableOpacity>
          <Text style={ styles.baseUnitBalanceText }>Bakiye {getBaseBalance}</Text>
        </View>
      </View>
    );
  }
  renderQuoteUnit() {
    const { getQuoteBalance, calculateQuoteAmount } = OperationForm;
    const { quote } = OperationStore;
    return (
      <View style={ styles.quoteUnitContainer }>
        <View style={ styles.baseUnitInputView }>
          <TextInput
            underlineColorAndroid="transparent"
            editable={ false }
            style={ styles.quoteUnitInput }
            value={ calculateQuoteAmount }
          />
        </View>
        <View style={ styles.baseUnitView }>
          <TouchableOpacity
            onPress={ () => UIC.openProductsModal('quote', quote) }
            style={ styles.baseUnitTouchableLabel }
          >
            <Text style={ styles.baseUnitLabel }>{quote}</Text>
            <Image
              uri="triangleGray"
              height={ Statics.size(11) }
              width={ Statics.size(14) }
            />
          </TouchableOpacity>
          <Text style={ styles.baseUnitBalanceText }>Bakiye {getQuoteBalance}</Text>
        </View>
      </View>
    );
  }
  render() {
    const {
      BTC,
      ETH,
      LTC,
      XRP,
      TRY,
    } = BinanceStore.currencies;
    if (!BTC || !ETH || !LTC || !XRP || !TRY &&
      OperationStore.calculatedUnitPrice === 0
    ) return UIC.isActiveScreen('HomeScreen') && <Indicator isForever />;
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <View style={ styles.progressBarView }>
            {OperationStore.shownUnitPrice > 0 &&
              <ProgressBar
                onTrigger={ () => {
                  OperationStore.getDisplayNewUnitPrice(true);
                } }
                width={ Statics.WIDTH }
              />
            }
          </View>
          {UIC.isActiveScreen('HomeScreen') && <CurrenciesModal />}
          {this.renderBackgroundColor()}
          <View style={ styles.contentContainer }>
            <View style={ styles.inputsContainer }>
              {this.renderBaseUnit()}
              <Image
                uri="triangleBlue"
                height={ Statics.size(15) }
                width={ Statics.size(18) }
              />
              {this.renderQuoteUnit()}
            </View>
          </View>
          <View style={ styles.buttonContainer }>
            <Button
              height={ Statics.size(56) }
              width={ Statics.WIDTH * 0.829 }
              label="DÖNÜŞTÜR"
              labelColor="white"
              backgroundColor={ Colors.navyBlue }
              fontSize={ Statics.size(21) }
              onPress={ () => {} }
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  progressBarView: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
    height: Statics.size(10),
    width: Statics.WIDTH,
  },
  contentContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: Statics.size(440),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  inputsContainer: {
    flex: 1,
    width: Statics.WIDTH,
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
    paddingBottom: Statics.size(20),
  },
  baseUnitContainer: {
    width: Statics.WIDTH * 0.914,
    height: Statics.size(70),
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    marginBottom: Statics.size(8),
    flexDirection: 'row',
  },
  quoteUnitContainer: {
    width: Statics.WIDTH * 0.914,
    height: Statics.size(70),
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    marginTop: Statics.size(8),
    marginBottom: Statics.size(20),
    flexDirection: 'row',
  },
  blueContainer: {
    width: Statics.WIDTH,
    height: Statics.size(280),
    backgroundColor: Colors.blue,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  unitPriceView: {
    width: Statics.WIDTH,
    height: Statics.size(120),
    marginBottom: Statics.size(50),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    alignSelf: 'center',
  },
  rowSection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  baseUnitInputView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: Statics.size(15),
  },
  baseUnitInput: {
    fontSize: Statics.size(32),
    color: Colors.navyBlue,
    width: '95%',
    paddingVertical: Statics.size(5),
    height: Statics.size(50),
  },
  baseUnitView: {
    flexDirection: 'column',
    flex: 2,
    paddingRight: Statics.size(15),
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  baseUnitTouchableLabel: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  baseUnitLabel: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    marginRight: Statics.size(20),
  },
  baseUnitBalanceText: {
    fontSize: Statics.size(16),
    color: '#BBC2D3',
  },
  quoteUnitInput: {
    fontSize: Statics.size(32),
    color: Colors.navyBlue,
    width: '95%',
    paddingVertical: Statics.size(5),
    height: Statics.size(50),
    opacity: 0.5,
  },
  unitPriceBaseTypeText: {
    fontSize: Statics.size(24),
    fontFamily: Fonts.PRegular,
    color: 'white',
  },
  unitPriceQuotePriceText: {
    alignSelf: 'center',
    minWidth: Statics.size(220),
    fontSize: Statics.size(50),
    color: 'white',
    marginRight: Statics.size(5),
    marginLeft: Statics.size(5),
  },
  unitPriceQuoteTypeText: {
    fontSize: Statics.size(24),
    fontFamily: Fonts.PRegular,
    color: 'white',
  },
});
