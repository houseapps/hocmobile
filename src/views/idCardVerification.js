import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';
import Swiper from 'react-native-swiper';
import { RNCamera } from 'react-native-camera';

import StepIndicator from '../components/stepIndicator';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import UserStore from '../stores/userStore';

import NavigationStore from '../stores/navigationStore';

import IDCardForm from '../stores/forms/idCardForm';

import NavigatorStyle from '../consts/navigatorStyle';

@observer
export default class IDCardVerification extends Component {
  componentDidMount() {
    IDCardForm.clearStates();
    IDCardForm.getDocuments();
  }
  static navigatorStyle = NavigatorStyle;
  renderCamera() {
    return (
      <RNCamera
        ref={ (ref) => { this.camera = ref; } }
        style={ styles.preview }
        type={ RNCamera.Constants.Type.back }
        flashMode={ RNCamera.Constants.FlashMode.off }
        permissionDialogTitle="Permission to use camera"
        permissionDialogMessage="We need your permission to use your camera phone"
      />
    );
  }
  renderItem(data, step) {
    let imageUri = null;
    if (data && typeof data === 'object') imageUri = data.uri;
    else imageUri = data;
    return (
      <View style={ styles.swiperItemContainer }>
        <Image
          height={ 250 }
          width={ 330 }
          uri="target"
          style={ styles.targetImage }
          resizeMode="stretch"
        />
        {IDCardForm.isCamera
          && IDCardForm.currentStep === step
          && this.renderCamera()
        }
        {data &&
        <Image
          height={ 220 }
          width={ 300 }
          base64data={ imageUri }
          resizeMode="contain"
        />
        }
      </View>
    );
  }
  renderEndOfSteps() {
    return (
      <View style={ styles.endOfSteps }>
        <Text style={ styles.infoText }>Doğrulama işlemi sonuçlandığında tarafınıza mail ile bilgilendirme yapılacaktır.</Text>
        <Image
          height={ 150 }
          width={ 100 }
          uri="searchUser"
          resizeMode="contain"
        />
      </View>
    );
  }
  renderBackStep() {
    const { currentStep, isCamera } = IDCardForm;
    if (currentStep === 0 || currentStep > 2 || isCamera) return <View />;
    else {
      return (
        <TouchableOpacity
          onPress={ () => {
            if (currentStep > 0) {
              this.refs.swiper.scrollBy(-1, false);
              IDCardForm.currentStep -= 1;
            }
          } }
        >
          <Text style={ styles.backStepLabel }>Geri</Text>
        </TouchableOpacity>
      );
    }
  }
  renderNextStep() {
    const { isNextStep } = IDCardForm;
    if (!isNextStep) return null;
    let buttonText = '';
    if (IDCardForm.currentStep < 2) buttonText = 'Devam et';
    else if (IDCardForm.isUpdate) buttonText = 'Güncelle';
    else buttonText = 'Kaydet';
    return (
      <TouchableOpacity
        onPress={ () => {
          if (IDCardForm.currentStep < 2) {
            this.refs.swiper.scrollBy(1, false);
            IDCardForm.currentStep += 1;
          } else {
            UserStore.verifyIDCard().then(() => {
              IDCardForm.currentStep += 1;
              IDCardForm.displayResult = true;
            });
          }
        } }
      >
        {IDCardForm.currentStep < 3 &&
          <Text style={ styles.nextStepLabel }>
            { buttonText }
          </Text>
        }
      </TouchableOpacity>
    );
  }
  render() {
    const { currentStep, displayResult, form } = IDCardForm;
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <Indicator />
          <View style={ styles.formContainer }>
            <View style={ styles.stepIndicatorView }>
              <StepIndicator currentPosition={ currentStep } />
            </View>
            {!displayResult ?
              <Swiper
                index={ 0 }
                ref="swiper"
                loop={ false }
                removeClippedSubviews={ false }
                showsButtons={ false }
                scrollEnabled={ false }
                showsPagination={ false }
                onIndexChanged={ () => {} }
                containerStyle={ styles.swiperContainer }
                width={ Statics.size(330) }
                height={ Statics.size(250) }
              >
                {this.renderItem(IDCardForm.form.fcardPhoto, 0)}
                {this.renderItem(IDCardForm.form.bcardPhoto, 1)}
                {this.renderItem(IDCardForm.form.sPhoto, 2)}
              </Swiper>
              :
              this.renderEndOfSteps()
            }
            {IDCardForm.currentStep < 3 &&
            <View>
              <Text style={ styles.littleInfoText }>Aşağıda bulunan 2 seçenek ile fotoğraf veya görsel yükleme yapabilirsiniz.</Text>
              <View style={ styles.iconsView }>
                <TouchableOpacity onPress={ () => IDCardForm.launchCamera() }>
                  <Image
                    uri="camera"
                    width={ Statics.size(40) }
                    height={ Statics.size(40) }
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={ () => IDCardForm.launchGallery() }>
                  <Image
                    uri="gallery"
                    width={ Statics.size(40) }
                    height={ Statics.size(40) }
                    style={ styles.galleryIcon }
                  />
                </TouchableOpacity>

              </View>
            </View>
            }
            <View style={ styles.routerView }>
              {this.renderBackStep()}
              {this.renderNextStep()}
            </View>
            {IDCardForm.currentStep === 3 &&
            <Button
              height={ Statics.size(56) }
              width={ Statics.WIDTH * 0.829 }
              label="GERİ DÖN"
              labelColor="white"
              backgroundColor={ Colors.navyBlue }
              fontSize={ Statics.size(21) }
              onPress={ () => NavigationStore.pop() }
            />
            }
            {IDCardForm.isCamera &&
            <View style={ styles.takePhotoButton }>
              <Button
                height={ Statics.size(56) }
                width={ Statics.WIDTH * 0.829 }
                label="FOTOĞRAF ÇEK"
                labelColor="white"
                backgroundColor={ Colors.navyBlue }
                fontSize={ Statics.size(21) }
                onPress={ () => IDCardForm.takePicture(this.camera) }
              />
            </View>
            }
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    marginTop: Statics.size(30),
    marginBottom: Statics.size(30),
    margin: Statics.size(10),
    height: Statics.HEIGHT - Statics.size(75),
    width: Statics.WIDTH - Statics.size(60),
    backgroundColor: 'transparent',
    borderRadius: Statics.size(4),
  },
  routerView: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: '90%',
    flex: 1,
    marginBottom: Statics.size(20),
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  nextStepLabel: {
    color: Colors.navyBlue,
    fontSize: Statics.size(15),
    fontFamily: Fonts.PBold,
  },
  backStepLabel: {
    color: Colors.navyBlue,
    fontSize: Statics.size(15),
    fontFamily: Fonts.PBold,
  },
  iconsView: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: Statics.size(40),
  },
  swiperContainer: {
    flex: 2,
    marginTop: Statics.size(30),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: Statics.size(330),
    height: Statics.size(250),
  },
  galleryIcon: {
    marginLeft: Statics.size(50),
  },
  stepIndicatorView: {
    padding: Statics.size(15),
    borderRadius: Statics.size(3),
    borderBottomWidth: 1,
    borderColor: 'rgba(32, 55, 112, 0.4)',
  },
  swiperItemContainer: {
    flex: 1,
    height: Statics.size(250),
    width: Statics.size(330),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  endOfSteps: {
    flex: 2,
    width: Statics.size(330),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoText: {
    color: Colors.navyBlue,
    fontFamily: Fonts.PBold,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: Statics.size(17),
  },
  preview: {
    height: Statics.size(220),
    width: Statics.size(300),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  targetImage: {
    position: 'absolute',
    top: 0,
    alignSelf: 'center',
    zIndex: -1,
  },
  takePhotoButton: {
    position: 'absolute',
    bottom: Statics.size(20),
    alignSelf: 'center',
  },
  littleInfoText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(16),
    marginBottom: Statics.size(10),
    alignSelf: 'center',
    textAlign: 'center',
  },
});
