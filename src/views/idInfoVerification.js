import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react/native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import BirthDayInput from '../components/birthDayInput';
import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import NavigationStore from '../stores/navigationStore';
import IDInfoForm from '../stores/forms/idInfoForm';
import UserStore from '../stores/userStore';

import NavigatorStyle from '../consts/navigatorStyle';

@observer
export default class IDInfoVerification extends Component {
  static navigatorStyle = NavigatorStyle;
  componentDidMount() {
    const { navigator } = this.props;
    NavigationStore.navigator = navigator;
  }
  renderForm() {
    const {
      tckn,
      birthday,
      name,
      surname,
    } = IDInfoForm.form;
    return (
      <View style={ styles.formMargin }>
        <TextInput
          label="T.C. Kimlik No"
          onChangeText={ value => IDInfoForm.handleUpdateForm('tckn', value) }
          returnKeyType="next"
          keyboardType="numeric"
          value={ tckn }
        />
        <BirthDayInput
          onConfirm={ value => IDInfoForm.handleUpdateForm('birthday', value) }
          value={ birthday }
          activeOpacity={ 0.9 }
        />
        <TextInput
          label="İsim"
          onChangeText={ value => IDInfoForm.handleUpdateForm('name', value) }
          returnKeyType="next"
          value={ name }
        />
        <TextInput
          label="Soyisim"
          onChangeText={ value => IDInfoForm.handleUpdateForm('surname', value) }
          returnKeyType="next"
          value={ surname }
        />
      </View>
    );
  }
  renderVerifyButton() {
    return (
      <View style={ styles.flex1 }>
        <Button
          height={ Statics.size(56) }
          width={ Statics.WIDTH * 0.829 }
          label="DOĞRULA"
          labelColor="white"
          backgroundColor={ Colors.navyBlue }
          fontSize={ Statics.size(21) }
          onPress={ () => UserStore.verifyIDInfo() }
          marginTop={ Statics.size(30) }
        />
      </View>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <Indicator />
          <View style={ styles.textContainer }>
            <Text style={ styles.titleText }>Kimlik Doğrulama</Text>
          </View>
          <View style={ styles.formContainer }>
            <KeyboardAwareScrollView
              contentContainerStyle={ styles.awareScrollView }
              keyboardDismissMode="interactive"
              enableOnAndroid
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={ false }
            >
              {this.renderForm()}
              {this.renderVerifyButton()}
            </KeyboardAwareScrollView>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  awareScrollView: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  formContainer: {
    flex: 4,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    width: Statics.WIDTH * 0.829,
    alignSelf: 'center',
  },
  titleText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(40),
  },
  flex1: {
    flex: 1,
    alignItems: 'center',
  },
  formMargin: {
    margin: Statics.size(10),
  },
});
