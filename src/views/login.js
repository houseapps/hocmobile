import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react/native';

import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import LoginForm from '../stores/forms/loginForm';
import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';

import { navigatorStyle } from '../consts/navigatorStyle';

@observer
export default class Login extends Component {
  static navigatorStyle = navigatorStyle;
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UIC.setCurrentlyVisibleScreen('LoginScreen');
  }
  componentDidMount() {
    const { navigator } = this.props;
    NavigationStore.navigator = navigator;
  }
  renderRegisterButton() {
    return (
      <TouchableOpacity
        activeOpacity={ 0.8 }
        style={ styles.registerButtonContainer }
        onPress={ () => NavigationStore.route('RegisterScreen', true) }
      >
        <Text style={ styles.registerText }>Hesabın mı yok? Hemen KAYIT OL</Text>
      </TouchableOpacity>
    );
  }
  renderForm() {
    const { passwordVisible, form } = LoginForm;
    const {
      email,
      password,
    } = form;
    return (
      <View style={ styles.formContainer }>
        <TextInput
          label="E-Posta"
          onChangeText={ value => LoginForm.handleUpdateForm('email', value) }
          keyboardType="email-address"
          value={ email }
          autoCapitalize="none"
        />
        <TextInput
          label="Parola"
          isVisible={ passwordVisible }
          onChangeText={ value => LoginForm.handleUpdateForm('password', value) }
          onPressEye={ () => LoginForm.changePasswordVisible() }
          value={ password }
          eyeState={ passwordVisible }
          eye
          autoCapitalize="none"
        />
        <TouchableOpacity
          style={ styles.forgotPasswordContainer }
          onPress={ () => NavigationStore.route('ResetPasswordScreen', true) }
        >
          <Text style={ styles.forgotPassword } >Parolamı Unuttum</Text>
        </TouchableOpacity>
      </View>
    );
  }
  renderLoginButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="GİRİŞ YAP"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => UserStore.login() }
      />
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          {UIC.isActiveScreen('LoginScreen') && <Indicator />}
          <View style={ styles.textContainer }>
            <Text style={ styles.titleText }>Giriş yap</Text>
          </View>
          {this.renderForm()}
          <View style={ styles.buttonContainer }>
            {this.renderLoginButton()}
            {this.renderRegisterButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    flex: 2,
    justifyContent: 'flex-start',
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: Statics.size(5),
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    width: Statics.WIDTH * 0.829,
    alignSelf: 'center',
  },
  titleText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(40),
  },
  forgotPassword: {
    fontSize: Statics.size(19),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginTop: Statics.size(15),
    textDecorationLine: 'underline',
  },
  registerButtonContainer: {
    alignSelf: 'center',
  },
  registerText: {
    padding: Statics.size(20),
    color: Colors.navyBlue,
    fontSize: Statics.size(18),
    fontFamily: Fonts.PRegular,
  },
  forgotPasswordContainer: {
    width: Statics.WIDTH * 0.40,
    height: Statics.size(60),
    alignSelf: 'flex-start',

  },
  inputContainerStyle: {
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    backgroundColor: 'white',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    marginBottom: Statics.size(20),
    width: Statics.WIDTH * 0.829,
    margin: Statics.size(10),
    alignSelf: 'center',
  },
});
