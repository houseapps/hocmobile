
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { observer } from 'mobx-react/native';

import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';
import Assets from '../helpers/assets';

import NavigationStore from '../stores/navigationStore';
import PaparaStore from '../stores/paparaStore';
import UserStore from '../stores/userStore';

import PaparaWithdrawForm from '../stores/forms/paparaWithdrawForm';
import TransactionForm from '../stores/forms/transactionForm';

import { navigatorStyle, leftButtons, rightButtons } from '../consts/navigatorStyle';

@observer
export default class PaparaWithdraw extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  renderAccountBalance() {
    const { getTry } = TransactionForm;
    return (
      <View>
        <View style={ styles.balanceTitleView }>
          <Text style={ styles.balanceText }>Hesap Bakiyeniz</Text>
        </View>
        <View style={ styles.balanceAmount }>
          <Text style={ styles.amount }>{getTry}</Text>
          <Text style={ styles.asset }>TRY</Text>
        </View>
      </View>
    );
  }
  renderForm() {
    const { amount, accountNumber } = PaparaWithdrawForm.form;
    return (
      <View>
        <TextInput
          label="Hesap Numarası"
          onChangeText={ value => PaparaWithdrawForm.handleUpdateForm('accountNumber', parseInt(value, 0)) }
          keyboardType="number-pad"
          value={ accountNumber }
        />
        <TextInput
          label="Tutar"
          onChangeText={ value => PaparaWithdrawForm.handleUpdateForm('amount', parseInt(value, 0)) }
          keyboardType="number-pad"
          value={ amount }
        />
      </View>
    );
  }
  renderDepositButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="ÇEK"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => PaparaStore.paparaWithdraw() }
      />
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <Indicator />
          <View style={ styles.infoContainer }>
            <View style={ styles.paparaIconView }>
              <Image
                uri="papara"
                height={ Statics.size(22) }
                width={ Statics.size(80) }
              />
              <Text style={ styles.paymentTypeText }>ile</Text>
            </View>
            <Text style={ styles.infoText }>Çekmek istediğiniz miktarı ve hesap numaranızı giriniz.</Text>
          </View>
          <View style={ styles.formContainer }>
            {this.renderAccountBalance()}
            {this.renderForm()}
            {this.renderDepositButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
    width: Statics.WIDTH,
  },
  paymentTypeText: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginLeft: Statics.size(10),
  },
  infoText: {
    fontSize: Statics.size(19),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginTop: Statics.size(20),
  },
  amount: {
    fontSize: Statics.size(40),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  asset: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
    marginLeft: Statics.size(10),
  },
  balanceText: {
    fontSize: Statics.size(20),
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
    marginBottom: Statics.size(10),
  },
  paparaIconView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  formContainer: {
    flex: 3,
    padding: Statics.size(20),
    margin: Statics.size(10),
    width: Statics.WIDTH,
    justifyContent: 'flex-start',
  },
  infoContainer: {
    flex: 1,
    padding: Statics.size(20),
    width: Statics.WIDTH,
  },
  balanceTitleView: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: Colors.navyBlue,
  },
  balanceAmount: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    margin: Statics.size(20),
  },
});
