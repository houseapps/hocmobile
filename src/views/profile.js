import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';


import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';

import IDCardForm from '../stores/forms/idCardForm';

import { navigatorStyle, rightButtons } from '../consts/navigatorStyle';
import UserStore from '../stores/userStore';

@observer
export default class Profile extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UserStore.fetchProfile();
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
  }
  renderItem(label, route, color, border = true, style = null) {
    return (
      <TouchableOpacity
        activeOpacity={ 1 }
        onPress={ () => {
          if (color === 'logout') UserStore.logout();
          else NavigationStore.route(route, false, {}, this.props.navigator, label);
        } }
        style={ [styles.itemContainer, style && style] }
      >
        <View style={ [border && styles.border, styles.buttonContainer] }>
          <View style={ [styles.colorIcon, { backgroundColor: Colors.profileColors[color] }] } />
          <Text style={ [styles.label, { color: Colors.navyBlue }] }>{label}</Text>
          {color !== 'logout' &&
            <Image
              style={ styles.triangleIcon }
              uri="triangleRightGray"
              width={ Statics.size(14) }
              height={ Statics.size(14) }
            />
          }
        </View>
      </TouchableOpacity>
    );
  }
  renderIDCardItem(label, route, color, border = true, style = null) {
    const processingStyle = { color: Colors.fadedBlack, opacity: 0.6 };
    const rejectedStyle = { color: Colors.red, opacity: 1 };
    const acceptedStyle = { color: Colors.green, opacity: 1 };

    const { getCardVerificationStatus } = IDCardForm;
    if (!getCardVerificationStatus) return null;
    let componentState = {
      clickable: true,
      labelStyle: {
        fontSize: Statics.size(24),
        fontFamily: Fonts.PRegular,
        marginLeft: Statics.size(20),
        color: Colors.navyBlue,
      },
      iconName: 'triangleRightGray',
      label: 'İşlem yapabilmek için kimlik kartı bilgilerinizi sisteme yüklenemiz gerekmektedir.',
    };
    const objectKey = !getCardVerificationStatus ? null : Object.keys(getCardVerificationStatus)[0];
    if (objectKey === '0') {
      componentState = {
        clickable: false,
        labelStyle: { ...componentState.labelStyle, ...processingStyle },
        label: 'Kimlik bilgileriniz başarıyla sisteme gönderildi. Onay beklemektesiniz.',
        iconName: '',
        color: Colors.fadedBlack,
        opacity: 0.6,
      };
    } else if (objectKey === '1') {
      componentState = {
        clickable: false,
        labelStyle: { ...componentState.labelStyle, ...acceptedStyle },
        label: 'Kimlik bilgileriniz başarıyla kaydedildi.',
        iconName: 'successful',
        color: Colors.green,
        opacity: 1,
      };
    } else if (objectKey === '2') {
      componentState = {
        clickable: true,
        labelStyle: { ...componentState.labelStyle, ...rejectedStyle },
        label: 'Kimlik bilgileriniz sistem tarafından reddedildi. Tekrar deneyiniz.',
        iconName: 'failure',
        color: Colors.red,
        opacity: 1,
      };
    }

    return (
      <TouchableOpacity
        activeOpacity={ 1 }
        onPress={ () => {
          if (componentState.clickable) {
            NavigationStore.route('IDCardVerificationScreen', true, {}, this.props.navigator);
          }
        } }
        style={ [styles.bottomItemContainer] }
      >
        <Text style={ styles.infoText }>
          {componentState.label}
        </Text>
        <View style={ [border && styles.border, styles.buttonContainer] }>
          <View style={ [styles.colorIcon, { backgroundColor: componentState.color }] } />
          <Text style={ [styles.label, { color: componentState.color }] }>
            {getCardVerificationStatus[objectKey] || label}
          </Text>
          <Image
            style={ styles.triangleIcon }
            uri={ componentState.iconName }
            width={ Statics.size(14) }
            height={ Statics.size(14) }
          />
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={ styles.container }>
        {this.renderItem('Profil Bilgilerim', 'ProfileInfoScreen', 'profileInfo', false)}
        {this.renderItem('Parola Değiştir', 'ChangePasswordScreen', 'changePassword', true)}
        {this.renderItem('Çıkış Yap', '', 'logout', true)}
        {this.renderIDCardItem('Kimlik Doğrulama', 'IDCardVerificationScreen', 'idVerification', true)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
  },
  bottomItemContainer: {
    alignSelf: 'center',
    width: '90%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
    height: Statics.size(100),
  },
  border: {
    borderTopWidth: 1,
    borderColor: '#BBC2D3',
  },
  infoText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(16),
    marginBottom: Statics.size(10),
  },
  colorIcon: {
    height: Statics.size(12),
    width: Statics.size(12),
    borderRadius: Statics.size(6),
  },
  label: {
    fontSize: Statics.size(24),
    fontFamily: Fonts.PRegular,
    marginLeft: Statics.size(20),
    color: Colors.navyBlue,
  },
  triangleIcon: {
    alignSelf: 'center',
    justifyContent: 'flex-end',
    position: 'absolute',
    right: 0,
  },
});
