import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { observer } from 'mobx-react/native';

import ProfileInfoItem from '../components/profileInfoItem';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import UserStore from '../stores/userStore';

import { leftButtons, rightButtons, navigatorStyle } from '../consts/navigatorStyle';

@observer
export default class ProfileInfo extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  componentDidMount() {
    NavigationStore.navigator = this.props.navigator;
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  render() {
    const { user: { user } } = UserStore;
    return (
      <View style={ styles.container }>
        <View style={ styles.content }>
          <View style={ styles.sectionTitle }>
            <Text style={ styles.title }>Kişisel Bilgiler:</Text>
          </View>
          <View style={ styles.userInfo }>
            <ProfileInfoItem title="Ad-Soyad:" value={ `${user.name.toUpperCase()} ${user.surname.toUpperCase()}` } />
            <ProfileInfoItem title="E-Posta" value={ user.email } />
            <ProfileInfoItem title="Telefon:" value={ user.phone } />
          </View>
          <View style={ styles.sectionTitle }>
            <Text style={ styles.title }>Hesap Bilgileri:</Text>
          </View>
          <View style={ styles.userInfo }>
            <ProfileInfoItem title="E-Posta Doğrulama:" value={ user.email_verified ? 'Doğrulandı' : 'Doğrulanmadı' } />
            <ProfileInfoItem title="Numara Doğrulama:" value={ user.phone_number_verified ? 'Doğrulandı' : 'Doğrulanmadı' } />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  content: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    width: '100%',
    height: Statics.size(90),
    padding: Statics.size(20),
  },
  userInfo: {
    marginBottom: Statics.size(40),
    justifyContent: 'flex-start',
    marginLeft: Statics.size(40),
  },
  sectionTitle: {
    width: '100%',
    height: Statics.size(40),
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderColor,
    marginBottom: Statics.size(20),
  },
  title: {
    fontSize: Statics.size(18),
    color: Colors.navyBlue,
    fontFamily: Fonts.PBold,
  },
});
