
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { observer } from 'mobx-react/native';

import CryptoModal from '../components/cryptoModal';
import Image from '../components/image';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import NavigationStore from '../stores/navigationStore';
import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';
import TransactionForm from '../stores/forms/transactionForm';

import { navigatorStyle, rightButtons } from '../consts/navigatorStyle';

@observer
export default class Purchases extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UserStore.fetchProfile();
  }
  route(type) {
    let route = null;
    let title = '';
    if (type === 0) {
      route = 'PaparaWithdrawScreen';
      title = 'Papara';
    }
    if (type === 1) {
      route = 'EFTWithdrawScreen';
      title = 'EFT / Havale';
    }
    if (type === 2) UIC.openCryptoModal();
    if (title !== '') NavigationStore.route(route, false, {}, this.props.navigator, title);
  }
  renderBackgroundColor() {
    const { getTry } = TransactionForm;
    return (
      <View style={ styles.blueContainer }>
        <View style={ styles.unitPriceView }>
          <Text style={ styles.unitPriceQuotePriceText }>{getTry} </Text>
          <Text style={ styles.unitPriceBaseTypeText }>TRY</Text>
        </View>
      </View>
    );
  }
  renderPaymentTypeButton(type, text) {
    return (
      <View style={ styles.paymentTypeContainer }>
        <TouchableOpacity
          style={ styles.paymentTypeTouchable }
          onPress={ () => { this.route(type); } }
        >
          <View style={ styles.paymentTypeTextContainer }>
            {type === 0 &&
              <Image
                style={ styles.paparaIcon }
                uri="papara"
                height={ Statics.size(22) }
                width={ Statics.size(80) }
              />
            }
            <Text style={ styles.paymentTypeText }>
              { text }
            </Text>
          </View>
          <View style={ styles.transformedTriangleIcon }>
            <Image
              uri="triangleGray"
              height={ Statics.size(11) }
              width={ Statics.size(14) }
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    const { navigator } = this.props;
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <CryptoModal screen="CryptoWithdrawScreen"nav={ navigator } />
          {this.renderBackgroundColor()}
          <View style={ styles.contentContainer }>
            <View style={ styles.paymentsTypeItemsContainer }>
              {this.renderPaymentTypeButton(0, 'ile')}
              {this.renderPaymentTypeButton(1, 'EFT / Havale ile')}
              {this.renderPaymentTypeButton(2, 'Crypto ile')}
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  contentContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: Statics.size(480),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  paymentsTypeItemsContainer: {
    flex: 2,
    width: Statics.WIDTH,
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'center',
  },
  paymentTypeContainer: {
    width: Statics.WIDTH * 0.914,
    height: Statics.size(70),
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    marginTop: Statics.size(6),
    marginBottom: Statics.size(8),
    flexDirection: 'row',
  },
  blueContainer: {
    width: Statics.WIDTH,
    height: Statics.size(280),
    backgroundColor: Colors.blue,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  unitPriceView: {
    width: Statics.WIDTH,
    height: Statics.size(50),
    justifyContent: 'center',
    alignItems: 'flex-end',
    flexDirection: 'row',
  },
  paymentTypeTouchable: {
    flexDirection: 'row',
    flex: 2,
    width: '100%',
    paddingRight: Statics.size(15),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  paymentTypeText: {
    fontSize: Statics.size(24),
    color: Colors.navyBlue,
  },
  unitPriceBaseTypeText: {
    fontSize: Statics.size(50),
    fontFamily: Fonts.PThin,
    color: 'white',
  },
  unitPriceQuotePriceText: {
    fontSize: Statics.size(50),
    color: 'white',
  },
  paparaIcon: {
    marginRight: Statics.size(15),
  },
  paymentTypeTextContainer: {
    flexDirection: 'row',
    marginLeft: Statics.size(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  transformedTriangleIcon: {
    transform: [{ rotate: '-90deg' }],
  },
});
