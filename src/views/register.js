import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react/native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import BackButton from '../components/backButton';
import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Checkbox from '../components/checkbox';
import Button from '../components/button';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import UIC from '../stores/uiControllerStore';

import RegisterForm from '../stores/forms/registerForm';
import UserStore from '../stores/userStore';

import { navigatorStyle } from '../consts/navigatorStyle';

@observer
export default class Register extends Component {
  static navigatorStyle = navigatorStyle;
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UIC.setCurrentlyVisibleScreen('RegisterScreen');
  }
  renderForm() {
    const { passwordVisible, passwordAgainVisible, form } = RegisterForm;
    const {
      name,
      surname,
      email,
      password,
      passwordAgain,
      ks,
      kvkk,
    } = form;
    const halfWidth = (Statics.size(Statics.WIDTH * 0.829 - Statics.size(10)) / 2);
    return (
      <View style={ styles.editedRow }>
        <View style={ styles.flexDirectionRow }>
          <TextInput
            label="İsim"
            onChangeText={ value => RegisterForm.handleUpdateForm('name', value) }
            inputStyle={ { width: halfWidth } }
            containerStyle={ { width: halfWidth } }
            returnKeyType="next"
            value={ name }
            halfWidth
          />
          <TextInput
            label="Soyisim"
            onChangeText={ value => RegisterForm.handleUpdateForm('surname', value) }
            inputStyle={ { width: halfWidth } }
            containerStyle={ { width: halfWidth } }
            returnKeyType="next"
            value={ surname }
            halfWidth
          />
        </View>
        <TextInput
          label="E-Posta"
          onChangeText={ value => RegisterForm.handleUpdateForm('email', value) }
          keyboardType="email-address"
          returnKeyType="next"
          value={ email }
          autoCapitalize="none"
        />
        <TextInput
          label="Parola"
          isVisible={ passwordVisible }
          onChangeText={ value => RegisterForm.handleUpdateForm('password', value) }
          onPressEye={ () => RegisterForm.changePasswordVisible(0) }
          returnKeyType="next"
          value={ password }
          eyeState={ passwordVisible }
          eye
          autoCapitalize="none"
        />
        <TextInput
          label="Parola Tekrar"
          isVisible={ passwordAgainVisible }
          onChangeText={ value => RegisterForm.handleUpdateForm('passwordAgain', value) }
          onPressEye={ () => RegisterForm.changePasswordVisible(1) }
          returnKeyType="done"
          value={ passwordAgain }
          eyeState={ passwordAgainVisible }
          eye
          autoCapitalize="none"
        />
      </View>
    );
  }
  renderCheckBoxes() {
    const { ks, kvkk } = RegisterForm.form;
    return (
      <View style={ styles.checkBoxesContainer }>
        <View style={ styles.checkBoxesContent }>
          <Checkbox
            onPress={ () => {
              RegisterForm.changeKS();
              Keyboard.dismiss();
            } }
            isSelected={ ks }
          />
          <TouchableOpacity>
            <Text style={ [styles.text, styles.underlineText] }>Kullanıcı Sözleşmesini</Text>
          </TouchableOpacity>
          <Text style={ styles.text }> okudum, kabul ediyorum</Text>
        </View>
        <View style={ styles.checkBoxesContent }>
          <Checkbox
            onPress={ () => {
              RegisterForm.changeKVKK();
              Keyboard.dismiss();
            } }
            isSelected={ kvkk }
          />
          <TouchableOpacity>
            <Text style={ [styles.text, styles.underlineText] }>KVKK Sözleşmesini</Text>
          </TouchableOpacity>
          <Text style={ styles.text }> okudum, kabul ediyorum</Text>
        </View>
      </View>
    );
  }
  renderRegisterButton() {
    return (
      <View style={ styles.flex1 }>
        <Button
          height={ Statics.size(56) }
          width={ Statics.WIDTH * 0.829 }
          label="KAYIT OL"
          labelColor="white"
          backgroundColor={ Colors.navyBlue }
          fontSize={ Statics.size(21) }
          onPress={ () => UserStore.register() }
          marginTop={ Statics.size(30) }
        />
      </View>
    );
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          <View style={ styles.textContainer }>
            <BackButton navigator={ this.props.navigator } />
            <Text style={ styles.titleText }>Kayıt ol</Text>
          </View>
          {UIC.isActiveScreen('RegisterScreen') && <Indicator />}
          <View style={ styles.formContainer }>
            <KeyboardAwareScrollView
              contentContainerStyle={ styles.awareScrollView }
              keyboardDismissMode="interactive"
              enableOnAndroid
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={ false }
            >
              {this.renderForm()}
              {this.renderCheckBoxes()}
              {this.renderRegisterButton()}
            </KeyboardAwareScrollView>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  editedRow: {
    width: Statics.WIDTH * 0.86,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  formContainer: {
    flex: 4,
    alignItems: 'center',
  },
  text: {
    color: Colors.navyBlue,
    fontFamily: Fonts.PRegular,
  },
  underlineText: {
    textDecorationLine: 'underline',
    marginLeft: Statics.size(15),
    fontFamily: Fonts.PBold,
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: Statics.WIDTH * 0.910,
    alignItems: 'center',
  },
  titleText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(40),
    marginLeft: Statics.size(20),
  },
  checkBoxesContainer: {
    flex: 1,
    paddingTop: Statics.size(10),
    paddingBottom: Statics.size(10),
    alignSelf: 'center',
  },
  checkBoxesContent: {
    flexDirection: 'row',
    paddingTop: Statics.size(15),
    alignItems: 'center',
  },
  flex1: {
    flex: 1,
    alignSelf: 'center',
  },
  flexDirectionRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: Statics.WIDTH * 0.829,
  },
});
