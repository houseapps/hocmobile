import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react/native';

import BackButton from '../components/backButton';
import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';
import Fonts from '../helpers/fonts';

import UIC from '../stores/uiControllerStore';

import ResetForm from '../stores/forms/resetForm';
import UserStore from '../stores/userStore';


@observer
export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UIC.setCurrentlyVisibleScreen('ResetPasswordScreen');
  }
  renderForm() {
    return (
      <TextInput
        label="E-Posta"
        onChangeText={ value => ResetForm.handleUpdateForm('email', value) }
        keyboardType="email-address"
        value={ ResetForm.form.email }
      />
    );
  }

  renderResetButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="E-POSTA GÖNDER"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => UserStore.reset() }
        marginTop={ Statics.size(30) }
      />
    );
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          {UIC.isActiveScreen('ResetPasswordScreen') && <Indicator />}
          <View style={ styles.textContainer }>
            <BackButton navigator={ this.props.navigator } />
            <Text style={ styles.titleText }>Parolamı Unuttum</Text>
          </View>
          <View style={ styles.formContainer }>
            {this.renderForm()}
            <Text style={ styles.description }>Üyeliğinizin bulunduğu mail adresinizi giriniz.</Text>
          </View>
          <View style={ styles.buttonContainer }>
            {this.renderResetButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  formContainer: {
    flex: 2,
    marginTop: Statics.size(10),
    marginBottom: Statics.size(10),
    width: Statics.WIDTH * 0.829,
  },
  description: {
    fontSize: Statics.size(18),
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
    marginTop: Statics.size(10),
    marginLeft: Statics.size(5),
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: Statics.size(67),
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: Statics.WIDTH * 0.910,
    alignItems: 'center',
  },
  titleText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(40),
    marginLeft: Statics.size(20),
  },
});
