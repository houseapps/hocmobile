import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import { observer } from 'mobx-react/native';

import TimeUpModal from '../components/timeUpModal';
import TextInput from '../components/textInput';
import Indicator from '../components/indicator';
import Button from '../components/button';
import Timer from '../components/timer';
import Text from '../components/text';

import * as Statics from '../helpers/statics';
import Colors from '../helpers/colors';

import SmsForm from '../stores/forms/smsForm';
import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';

import { NavigatorStyle } from '../consts/navigatorStyle';

@observer
export default class SmsVerification extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') UIC.setCurrentlyVisibleScreen('SmsVerificationScreen');
  }
  static navigatorStyle = NavigatorStyle;
  renderPhoneButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="KOD GÖNDER"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => UserStore.sendCode() }
      />
    );
  }
  renderSmsButton() {
    return (
      <Button
        height={ Statics.size(56) }
        width={ Statics.WIDTH * 0.829 }
        label="DOĞRULA"
        labelColor="white"
        backgroundColor={ Colors.navyBlue }
        fontSize={ Statics.size(21) }
        onPress={ () => UserStore.verifyCode() }
      />
    );
  }
  renderPhoneStep() {
    const { phone } = SmsForm;
    return (
      <View style={ styles.formContainer }>
        <View style={ styles.phoneContainer }>
          <Text style={ styles.inputLabel }>Telefon Numaranız:</Text>
          <View style={ styles.maskedInputContainerStyle }>
            <TextInputMask
              ref="input"
              style={ styles.maskedInputStyle }
              onChangeText={ (value) => {
                SmsForm.handleUpdateForm('phone', value);
              } }
              type="cel-phone"
              value={ phone }
              maxLength={ 14 }
              options={
                {
                  dddMask: '(999) 999 ',
                }
              }
              underlineColorAndroid="transparent"
            />
          </View>
        </View>
        {this.renderPhoneButton()}
      </View>
    );
  }
  renderSmsStep() {
    const { code } = SmsForm;
    return (
      <View style={ styles.formContainer }>
        <Timer
          state={ UIC.timer }
          onEnd={ () => SmsForm.timeUp() }
        />
        <TextInput
          label="Doğrulama Kodu"
          labelStyle={ styles.labelStyle }
          textStyle={ styles.textStyle }
          inputStyle={ styles.inputStyle }
          keyboardType="numeric"
          value={ code }
          onChangeText={ (value) => {
            SmsForm.handleUpdateForm('code', value);
          } }
        />
        {this.renderSmsButton()}
      </View>
    );
  }
  render() {
    const { step } = SmsForm;
    return (
      <TouchableWithoutFeedback
        onPress={ () => Keyboard.dismiss() }
      >
        <View style={ styles.container }>
          {UIC.isActiveScreen('SmsVerificationScreen') && <TimeUpModal />}
          {UIC.isActiveScreen('SmsVerificationScreen') && <Indicator />}
          <View style={ styles.textContainer }>
            <Text style={ styles.titleText }>
              Sms Doğrulama
            </Text>
            <Text style={ styles.infoText }>
              Doğrulama kodu almak için lütfen telefon numaranızı giriniz.
            </Text>
          </View>
          {step === 1 ?
            this.renderPhoneStep()
            :
            this.renderSmsStep()
          }
          <View style={ styles.bottomSpace } />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  formContainer: {
    flex: 3,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(40),
  },
  infoText: {
    color: Colors.navyBlue,
    fontSize: Statics.size(16),
    marginTop: Statics.size(10),
  },
  labelStyle: {
    color: Colors.navyBlue,
    opacity: 0.5,
  },
  textStyle: {
    color: Colors.navyBlue,
  },
  inputStyle: {
    backgroundColor: 'white',
  },
  textContainer: {
    flex: 2,
    justifyContent: 'center',
    marginBottom: Statics.size(10),
    width: Statics.WIDTH * 0.829,
  },
  maskedInputStyle: {
    height: Statics.size(60),
    width: Statics.WIDTH * 0.829,
    fontSize: Statics.size(20),
    color: Colors.navyBlue,
    paddingHorizontal:
    Statics.size(10),
  },
  phoneContainer: {
    justifyContent: 'flex-start',
  },
  inputLabel: {
    alignSelf: 'flex-start',
    fontSize: Statics.size(16),
    color: Colors.navyBlue,
    marginBottom: Statics.size(5),
  },
  maskedInputContainerStyle: {
    width: Statics.WIDTH * 0.829,
    height: Statics.size(60),
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: Statics.size(4),
    borderColor: 'transparent',
    borderBottomWidth: 0,
    shadowColor: '#0D4681',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    elevation: 9,
    marginBottom: Statics.size(8),
  },
  bottomSpace: {
    flex: 3,
  },
});
