
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
} from 'react-native';
import { observer } from 'mobx-react/native';

import TransactionDetailItem from '../components/transactionDetailItem';
import Image from '../components/image';
import Text from '../components/text';

import Colors from '../helpers/colors';
import Assets from '../helpers/assets';

import * as Statics from '../helpers/statics';
import TransactionStore from '../stores/transactionStore';
import Fonts from '../helpers/fonts';

import { leftButtons, rightButtons } from '../consts/navigatorStyle';


@observer
export default class TransactionDetail extends Component {
  static navigatorButtons = { leftButtons, rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.type === 'NavBarButtonPress' && event.id === 'back') {
      const { navigator } = this.props;
      navigator.pop();
    }
  }
  renderItem(data) {
    return <TransactionDetailItem data={ data } />;
  }

  render() {
    const { transactionDetail } = TransactionStore;
    const { selectedTransaction } = TransactionStore;

    return (
      <View style={ styles.container }>
        <View style={ styles.sectionTitle }>
          <Image
            style={ styles.detailLogo }
            uri={ selectedTransaction.toLowerCase() }
            width={ Statics.size(14) }
            height={ Statics.size(14) }
          />
          <Text style={ styles.assetName }>{ selectedTransaction } Hesap Hareketleri </Text>
        </View>
        {
          (transactionDetail && transactionDetail.length > 0) ?
            <FlatList
              style={ styles.flatlist }
              data={ transactionDetail }
              renderItem={ ({ item }) => this.renderItem(item) }
            />
            :
            <View style={ styles.emptyContainer }>
              <Text style={ styles.noAction }>
                Herhangi bir işleminiz bulunmamaktadır
              </Text>
            </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: Statics.size(16),
    paddingRight: Statics.size(16),
  },
  detailLogo: {
    height: Statics.size(36),
    width: Statics.size(36),
    marginRight: Statics.size(5),
  },
  sectionTitle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: Statics.size(70),
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderColor,
  },
  assetName: {
    fontSize: 24,
    color: Colors.navyBlue,
    fontFamily: Fonts.PLight,
  },
  emptyContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: Statics.size(50),
    width: '100%',
    height: '100%',
  },
  noAction: {
    fontSize: Statics.size(16),
    fontFamily: Fonts.PBold,
    color: Colors.navyBlue,
  },
});
