import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
} from 'react-native';

import { observer } from 'mobx-react/native';

import TransactionItem from '../components/transactionItem';
import Indicator from '../components/indicator';

import TransactionStore from '../stores/transactionStore';
import NavigationStore from '../stores/navigationStore';
import BinanceStore from '../stores/binanceStore';
import UIC from '../stores/uiControllerStore';
import UserStore from '../stores/userStore';

import TransactionForm from '../stores/forms/transactionForm';

import * as Statics from '../helpers/statics';

import { navigatorStyle, rightButtons } from '../consts/navigatorStyle';

@observer
export default class TransactionList extends Component {
  static navigatorStyle = navigatorStyle;
  static navigatorButtons = { rightButtons };
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.id === 'willAppear') {
      UserStore.fetchProfile();
      UIC.setCurrentlyVisibleScreen('TransactionsList');
    }
  }
  componentDidMount() {
    TransactionStore.fetchTransactionAssets();
    NavigationStore.navigator = this.props.navigator;
  }
  renderItem(data) {
    const { asset, final_balance } = data;
    return <TransactionItem asset={ asset } final_balance={ final_balance } navigator={ this.props.navigator } />;
  }
  render() {
    const { transactionsByValue } = TransactionForm;
    const {
      BTC,
      ETH,
      LTC,
      XRP,
      TRY,
    } = BinanceStore.currencies;
    if (!BTC || !ETH || !LTC || !XRP) {
      return UIC.isActiveScreen('TransactionsList') && <Indicator isForever />;
    }
    return (
      <View style={ styles.container }>
        <FlatList
          style={ styles.flatlist }
          data={ transactionsByValue }
          renderItem={ ({ item }) => this.renderItem(item) }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: Statics.WIDTH,
  },
  flatlist: {
    width: Statics.WIDTH,
  },
});
